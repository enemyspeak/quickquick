
-- This is videogame

function love.load()
    math.randomseed(tonumber(tostring(os.time()):reverse():sub(1,6)))   
    for i=1, 4 do math.random() end

    io.output(io.stdout)
    io.stdout:setvbuf("no")
    lg = love.graphics
    HASFOCUS = true

    IRESX = 1280
    IRESY = 720
    scaleCanvas = love.graphics.newCanvas(IRESX,IRESY)

    local WIDTH = lg.getWidth()
    local HEIGHT = lg.getHeight()
    if WIDTH < IRESX or HEIGHT < IRESY then
        local w = WIDTH/IRESX
        local h = HEIGHT/IRESY
        if w > h then
            ISCALE = HEIGHT/IRESY
        else
            ISCALE = WIDTH/IRESX
        end
    elseif IRESX < WIDTH or IRESY < HEIGHT then
        local w = IRESX/WIDTH
        local h = IRESY/HEIGHTn
        if w > h then
            ISCALE = IRESY/HEIGHT
        else
            ISCALE = IRESX/WIDTH
        end
    else
        ISCALE = 1
    end

    love.graphics.setDefaultFilter("linear","linear")
    -- love.graphics.setPointStyle("smooth")
    love.graphics.setLineStyle("smooth")
    love.graphics.setLineJoin("bevel")
    love.graphics.setPointSize(1)
    love.graphics.setLineWidth(1)
    love.mouse.setVisible(false)

                    require 'lib.middleclass'
    Stateful =      require 'lib.stateful.stateful'
    flux =          require 'lib.flux'
    lume =          require 'lib.lume'
    bump =          require 'lib.bump'
                    require 'lib.Tserial'
                    require 'lib.ellipse'
    -- Postshader =    require 'lib.postshader'
    -- postshader = Postshader()
    -- postshader:refreshScreenSize(IRESX, IRESY)

    overstate = {} -- the sad reality of my bad code is this shit right here.
    -- I am so ashamed

    joysticks = {}
    Dualshock = require 'lua.dualshock'
    dualshocks = {}
    Cursor = require 'lua.cursor'
    mouseCursor = Cursor:new({})

    refreshJoysticks()

    require 'lua.state.gamestate'
    
    require 'lua.state.intro'
    require 'lua.state.test'
    require 'lua.state.options'
    
    gamestate = Gamestate:new()
    gamestate:gotoState("Intro")
end

function love.resize( rw, rh )
    local WIDTH = rw
    local HEIGHT = rh
    if WIDTH < IRESX or HEIGHT < IRESY then
        local w = WIDTH/IRESX
        local h = HEIGHT/IRESY
        if w > h then
            ISCALE = HEIGHT/IRESY
        else
            ISCALE = WIDTH/IRESX
        end
    elseif IRESX < WIDTH or IRESY < HEIGHT then
        local w = IRESX/WIDTH
        local h = IRESY/HEIGHT
        if w > h then
            ISCALE = IRESY/HEIGHT
        else
            ISCALE = IRESX/WIDTH
        end
    else
        ISCALE = 1
    end
    gamestate:resize(w,h)
end

function refreshJoysticks()
    local n = love.joystick.getJoystickCount()
    if #joysticks == n then

    elseif #joysticks < n then
        
    elseif #joysticks > n then

    end
end

function love.update(dt)
    love.window.setTitle(love.timer.getFPS())

--  if HASFOCUS then
        flux.update(dt)
        local mx,my = love.mouse.getPosition()
        mouseCursor:setX(mx)
        mouseCursor:setY(my)
        mouseCursor:update(dt)
        gamestate:update(dt)
--  end
end

function love.draw()
    -- scaleCanvas:clear()
    -- love.graphics.clear( )
    love.graphics.setCanvas(scaleCanvas)
        love.graphics.clear( )

        gamestate:draw()
    love.graphics.setCanvas()
    lg.setBackgroundColor(25,25,31)
    lg.setColor(255,255,255)
    love.graphics.draw(scaleCanvas,0,0,0,ISCALE,ISCALE)
    mouseCursor:draw()
end

function love.focus(f)
    HASFOCUS = f
end

function love.joystickadded(joystick)
    print("Joystick connected.")
    gamestate:joystickadded(joystick)
end

function love.joystickremoved(joystick)
    print("Joystick removed.")
    for i=1,#joysticks do
        if joysticks[i] == joystick then
            gamestate:joystickremoved(i)
            --dualshocks[joystick]:setDisconnected(true)
            joysticks[i] = nil
        end
    end
end

function love.gamepadpressed(joystick,button)
    gamestate:joystickpressed(joystick, button)
end

function love.joystickpressed(joystick, button)
    gamestate:joystickpressed(joystick, button)
end

function love.joystickreleased(joystick, button)
    gamestate:joystickreleased(joystick, button)
end

function love.keypressed(key, unicode)
    if key == "3" then
        local s = love.graphics.newScreenshot()
        s:encode("game4"..os.time()..".png")
    end
    if key == "escape" then
        love.event.push('quit')
    end
    gamestate:keypressed(key, unicode)
end

function love.keyreleased(key)
    gamestate:keyreleased(key)
end

function love.mousepressed(x, y, button)
    gamestate:mousepressed(x, y, button)
end

function love.mousereleased(x, y, button)
    gamestate:mousereleased(x, y, button)
end

function love.quit()
    gamestate:quit()
end
