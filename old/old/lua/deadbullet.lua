		
local DeadBullets = class('DeadBullets')

DeadBullets.static.BULLET = lg.newImage("res/leaf.png")

function DeadBullets:initialize(attributes)
	local attributes = attributes or {}
	self.offsetX = attributes.offsetX or 0
	self.offsetY = attributes.offsetY or 0

	self.x = attributes.x or 1
	self.y = attributes.y or 1
	self.width = 4
	self.height = 4
	
	self.owner = attributes.owner or 0

	self.time = math.random(0,math.pi*2)

	self.velocity = 360
	self.direction = attributes.direction or 1

	self.scaleX = math.random(0,math.pi*2)
	self.scaleY = math.random(0,math.pi*2)
end

function DeadBullets:getPosition( ... )
	return self.x, self.y
end

function DeadBullets:update(dt)	
	self.scaleX = self.scaleX + dt * 8
	self.scaleY = self.scaleY + dt * 8

	local sinTime = 8
	local sinMult = 15
	self.time = self.time + dt * sinTime
	if self.direction == 1 then
		local sinMult = sinMult + 10
		self.x = self.x + (math.sin(self.time)*sinMult)*dt
		self.y = self.y - self.velocity*dt
	elseif self.direction == 2 then
		self.y = self.y + (math.sin(self.time)*sinMult)*dt
		self.x = self.x + self.velocity*dt
	elseif self.direction == 3 then
		local sinMult = sinMult + 10
		self.x = self.x + (math.sin(self.time)*sinMult)*dt
		self.y = self.y + self.velocity*dt
	elseif self.direction == 4 then
		self.y = self.y + (math.sin(self.time)*sinMult)*dt
		self.x = self.x - self.velocity*dt
	end
end

function DeadBullets:draw()
	local sx = (math.sin(self.scaleX) + 1)
	local sy = (math.sin(self.scaleY) + 1)
	lg.setColor(255,255,255,255)
    lg.rectangle("fill",self.x-self.width/2-(sx/2),
                       self.y-self.height/2-(sy/2)-16,
                       self.width+(sx/2),
                       self.height+(sy/2))
	--[[
	lg.draw(DeadBullets.BULLET,self.x,
						self.y,
						0,
						(sx/2),
						(sy/2),
						DeadBullets.BULLET:getWidth()/2,
						DeadBullets.BULLET:getHeight()/2
						)
	--]]
end

return DeadBullets
