
local Level = class('level')

Level.static.Block = require('lua.block')
Level.static.Plant = require('lua.plant')

Level.static.BLOCKWIDTH = 16
Level.static.BLOCKHEIGHT = 16

Level.static.MAXPLANTS = 20
Level.static.MAXSPAWNS = 6

Level.static.TWEENTIME = 0.5

function Level:initialize(attributes)
	attributes = attributes or {}

	self.x = -16
	self.y = -16

	self.tween = 	{
					x = 0,
					y = 0
					}

	self.scroll = attributes.scroll or 0

	if self.scroll == 3 then 
		self.tween.y = lg.getHeight()
	elseif self.scroll == 4 then
		self.tween.x = -lg.getWidth()
	elseif self.scroll == 1 then
		self.tween.y = -lg.getHeight()
	elseif self.scroll == 2 then
		self.tween.x = lg.getWidth()
	end

	flux.to(self.tween,Level.TWEENTIME,{x = 0, y = 0}):ease("quadinout")

	self.grid = {}
	self.spawns = {}
	self.plants = {}
	self.players = {}
	self.deadplayers = {}
	self.bullets = {}
	self.playerHits = {} -- so we queue up the hits here, then dump each frame

	self.finshed = false

	self.width = attributes.width or 12
	self.height = attributes.height or 7

	self.world = bump.newWorld(Level.BLOCKHEIGHT)

	self:createGrid()
end

function Level:createGrid()
	local grid = {}
	local newGrid = {}
	local aliveChance = 0.5
	local function createCellmap()
		local g = {}
		for i = 1, self.width do
			g[i] = {}
			for k = 1, self.height do
				local n = math.random()
				if n < aliveChance then
					g[i][k] = true
				end
			end
		end
		return g
	end
	local function countNeighbors(map,x,y)
		local count = 0
		for j = -1, 1 do
			for k = -1, 1 do
				local neighborX = x+j
				local neighborY = y+k
				if j == 0 and k == 0 then
					-- nothing here
				elseif neighborX < 1 or neighborY < 1 or neighborX > self.width or neighborY > self.height then
					count = count + 1
				elseif map[neighborX][neighborY] then
					count = count + 1
				end
			end
		end
		return count
	end
	local function doSimulationStep(g)
		local deathLimit = 4
		local birthLimit = 4
		newGrid = g
		for x = 1, self.width do
			for y = 1, self.height do
				local count = countNeighbors(grid,x,y)
				if grid[x][y] then
					if count < deathLimit then
						newGrid[x][y] = false
					else
						newGrid[x][y] = true
					end
				else
					if count > birthLimit then
						newGrid[x][y] = true
					else
						newGrid[x][y] = false
					end
				end
			end
		end
		return newGrid
	end
	local function createBorder(g)
		local grid = g
		for i = 1,#grid[1] do
	    	grid[1][i] = true    
	    	grid[self.width][i] = true
	    end
	    for i = 1,#grid do
	    	grid[i][1] = true
	    	grid[i][self.height] = true
	    end
	    return grid
	end
	local function testLevel(g)
		local temp = false
		local grid = g
		local n = 0
		local maxWalls = 950 --1100
		local minWalls = 675
		for i,v in ipairs(grid) do
			for j,k in ipairs(grid) do
				if grid[i][j] then
					n = n + 1
				end
			end
		end
		if n > maxWalls then
			temp = true
		elseif n < minWalls then
			-- temp = true  -- don't need a minimum, I think
		end
		
		return temp	
--[[	local heightMap = {}  -- recursive pathfinding (not needed anymore)
		local sx = 1
		local sy = 1
		local distance = 1
		local toExamine = {}

		for i,v in ipairs(heightMap) do
			heightMap[i] + {}
			for j,k in ipairs(heightMap[i]) do
				heightMap[i][j] = distance
			end
		end

		table.insert(toExamine,{x = sx,y = sy})
		while #toExamine > 0 do
			local ex = toExamine.x
			local ey = toExamine.y
			local neighborDistance = heightMap[ex][ey] + 1

			local toExamineNeighbors = {}
			if ex - 1 >= 1 then
				table.insert(toExamineNeighbors,{x = ex - 1,y = ey})
			elseif self.grid[self.width][ey]:getN() == 16 then
				table.insert(toExamineNeighbors,{x = self.width,y = ey})
			end
			if ey - 1 >= 1 then
				table.insert(toExamineNeighbors,{x = ex,y = ey - 1})
			elseif self.grid[ex][self.height]:getN() == 16 then
				table.insert(toExamineNeighbors,{x = ex,y = self.height})
			end
			if ex + 1 <= self.width then
				table.insert(toExamineNeighbors,{x = ex + 1,y = ey})
			elseif self.grid[1][ey]:getN() == 16 then
				table.insert(toExamineNeighbors,{x = 1,y = ey})
			end
			if ey + 1 <= self.height then
				table.insert(toExamineNeighbors,{x = ex,y = ey + 1})
			elseif self.grid[ex][1]:getN() == 16 then
				table.insert(toExamineNeighbors,{x = ex,y = 1})
			end

			for i,v in ipairs(toExamineNeighbors) do
				local nx = toExamineNeighbors[i].x
				local ny = toExamineNeighbors[i].y
				local temp = true
				if value == 2 then 
					for i,v in ipairs(self.players) do
						local x,y = self.players[i]:getPosition()
						if x == nx and y == ny then 
							temp = false
						end
					end
				end
				if self.grid[nx][ny]:getN() == 16 and self.grid[nx][ny]:getHeight(value) == 0 or self.grid[nx][ny]:getHeight(value) > neighborDistance then
					if value == 1 or temp then
						self.grid[nx][ny]:setHeight(value,neighborDistance)
						table.insert(toExamine,{x = nx,y = ny})
					end
				end
			end

			table.remove(toExamine,1)
		end --]]
	end
	local function createSpawn(g)
		local grid = g
		local spawnHideLimit = 4
		while true do
			local i = math.random(1,#grid)
			local j = math.random(1,#grid[1])
			local t = false
			if grid[i][j] == true then
				t = true
			end
			if j - 1 < 1 then else
				if grid[i][j-1] == true then
					t = true
				end
			end
			for n,m in ipairs(self.spawns) do
				if (i-1)*Level.BLOCKWIDTH == self.spawns[n].x and (j-1)*Level.BLOCKHEIGHT == self.spawns[n].y then
					t = true
				end
			end
			if t then 
			else
				local count = countNeighbors(grid,i,j)
				if count > spawnHideLimit then
					i = i - 1
					j = j - 1
					return {x = i*Level.BLOCKWIDTH, y = j*Level.BLOCKHEIGHT}
				end
			end
		end
	end

	local temp = true
	while temp do
		grid = createCellmap()
		local maxGenerations = 1
		for i = 1,maxGenerations do
			grid = doSimulationStep(grid)
		end
		grid = createBorder(grid)
		temp = testLevel(grid) 	-- QA
		if temp then else
			for i = 1, Level.MAXSPAWNS do
				self.spawns[i] = createSpawn(grid)
			end
		end
	end

	for i = 1, self.width do 	-- create map for real
		self.grid[i] = {}
		for k = 1, self.height do
			if grid[i][k] == true then
				self.grid[i][k] = Level.Block:new({n = 1,offsetX = self.x, offsetY = self.y, x = i, y = k, width = Level.BLOCKWIDTH, height = Level.BLOCKHEIGHT})	
			else
				self.grid[i][k] = Level.Block:new({n = 16,offsetX = self.x, offsetY = self.y, x = i, y = k, width = Level.BLOCKWIDTH, height = Level.BLOCKHEIGHT})	
			end
		end
	end

--[[local maxCarves = 64 		-- old map generation (not cellular, random rectangle carving)
	for i=1, maxCarves do
		local w = math.random(2,6)
		local h = math.random(2,6)
		local x = math.random(1,self.width)
		local y = math.random(1,self.height)

		for i = x, x+w do
			if i > self.width or i < 1 then break end
			for k = y, y+h do
				if k > self.height or k < 1 then break end
				self.grid[i][k]:setN(16)
			end
		end
	end 
--]]

	for i,v in ipairs(self.grid) do
		for j,k in ipairs(self.grid[i]) do
			if self.grid[i][j]:getN() ~= 16 then
				local ground_tile = {
									x = i,
									y = j,
									w = Level.BLOCKWIDTH,
									h = Level.BLOCKHEIGHT
									}

				self.world:add(	ground_tile,
								self.x+(i)*ground_tile.w,
								self.y+(j+1)*ground_tile.h,
								ground_tile.w,
								ground_tile.h
								)
			end
		end
	end

	for i=1, Level.MAXPLANTS do
        local w = math.random(2,6)
        local h = math.random(2,6)
        local x = math.random(1,self.width)
        local y = math.random(1,self.height)

        for i = x, x+w do
        	for j = y, y+h do
            	if i > self.width or i < 1 then break end
            	if j > self.height or j < 1 then break end

				if self.grid[i][j]:getN() ~= 16 then
            		self.plants[#self.plants+1] = Level.Plant:new({offsetX = self.x, offsetY = self.y, x = i, y = j})
            	end
            end
        end
    end

	self:updateGridTilemapping()
end

function Level:getSpawnPoint()
	local n = math.random(1,#self.spawns)
	local sx = self.spawns[n].x
	local sy = self.spawns[n].y

	return sx,sy
end

function Level:addPlayer(n,x,y)
	self.players[n] = 	{
						x = x,
						y = y,
						w = 16,
						h = 32,
						isPlayer = true
						}
	self.world:add(self.players[n],x,y,16-1,32-1)
end

function Level:addDeadPlayer(n,x,y)
	self.deadplayers[n] = 	{
							x = x,
							y = y,
							w = 16,
							h = 32,
							isDeadPlayer = true
							}
	self.world:add(self.deadplayers[n],x,y,16-1,32-1)
end

function Level:addBullet(n,x,y,owner)
	self.bullets[n] = 	{
						x = x,
						y = y,
						w = 4,
						h = 4,
						isBullet = true,
						owner = owner
						}
	self.world:add(self.bullets[n],x,y,4,4)
end

function Level:updatePlayer(n,goalX,goalY)
	local actualX, actualY, cols, len 

	local playerFilter = function(item,other)
		if other.isBullet then return "cross" 
		elseif other.isDeadPlayer then return "cross"
		else return "slide" 
		end
	end

	actualX, actualY, cols, len = self.world:move(self.players[n], goalX, goalY,playerFilter)
	
 	for i=1,len do
 	 	if cols[i].other.isBullet then
 	 		local owner = cols[i].other.owner
 	 		table.insert(self.playerHits,{n = n, owner = owner})
 	 		-- ow
 	 	end
 	end
	return actualX,actualY
end

function Level:warpPlayer(n,goalX,goalY)
	local playerFilter = function(item,other) return "cross" end
	actualX, actualY, cols, len = self.world:move(self.players[n], goalX, goalY,playerFilter)

	return actualX,actualY
end

function Level:updateBullet(n,goalX,goalY)
	local actualX, actualY, cols, len 

	local bulletFilter = function(item,other)
		if other.isPlayer then return "cross" 
		elseif other.isDeadPlayer then return "cross"
		else return "touch" 
		end
	end

	actualX, actualY, cols, len = self.world:move(self.bullets[n], goalX, goalY,bulletFilter)
	local temp = false

 	for i=1,len do
 	 	if cols[i].other.isPlayer then
 	 	 	-- player hit
 	 	 	-- bullets go through players, ok?
 	 	else
 	 		temp = true
 	 	end
 	end
	return temp
end

function Level:updateDeadPlayer(n,goalX,goalY)
	local actualX, actualY, cols, len 

	local playerFilter = function(item,other)
		if other.isBullet then return "cross" 
		elseif other.isDeadPlayer then return "cross"
		elseif other.isPlayer then return "cross"
		else return "slide" 
		end
	end

	actualX, actualY, cols, len = self.world:move(self.deadplayers[n], goalX, goalY,playerFilter)
	
	return actualX,actualY
end

function Level:removeBullet(n)
	self.world:remove(self.bullets[n])
end

function Level:removePlayer(n)
	self.world:remove(self.players[n])
end

function Level:checkGrid(x,y) -- checks if x,y are empty space
	local gx = math.floor(x/16)+1
	local gy = math.floor(y/16)+1

	if self.grid[gx][gy]:getN() == 16 then
		return true
	end

	return 0
end

function Level:scrollLevel(d)
	local finish = function()
		self.finished = true
	end

	if d == 1 then 
		flux.to(self.tween,Level.TWEENTIME,{y = lg.getHeight()}):ease("quadinout"):oncomplete(finish)
	elseif d == 2 then
		flux.to(self.tween,Level.TWEENTIME,{x = -lg.getWidth()}):ease("quadinout"):oncomplete(finish)
	elseif d == 3 then
		flux.to(self.tween,Level.TWEENTIME,{y = -lg.getHeight()}):ease("quadinout"):oncomplete(finish)
	elseif d == 4 then
		flux.to(self.tween,Level.TWEENTIME,{x = lg.getWidth()}):ease("quadinout"):oncomplete(finish)
	end
end

function Level:updateGridTilemapping()
	for i,v in ipairs(self.grid) do
		for j,k in ipairs(self.grid[i]) do
			local value = 0

			if self.grid[i][j]:getN() == 16 then else
				if j+1 < self.height+1 then
					if self.grid[i][j+1]:getN() == 16 then else
						value = value + 4
					end
				end
				if j-1 > 0 then
					if self.grid[i][j-1]:getN() == 16 then else
						value = value + 1
					end
				end
				if i+1 < self.width+1 then 
					if self.grid[i+1][j]:getN() == 16 then else
						value = value + 2
					end
				end
				if i-1 > 0 then
					if self.grid[i-1][j]:getN() == 16 then else
						value = value + 8
					end
				end

				self.grid[i][j]:setN(value)
			end
		end
	end
end

function Level:getFinished()
	return self.finished
end

function Level:updateGrid(dt)
	for i,v in ipairs(self.grid) do
		for j,k in ipairs(self.grid[i]) do
			self.grid[i][j]:update(dt)
		end
	end
end

function Level:drawGrid()
	for i,v in ipairs(self.grid) do
		for j,k in ipairs(self.grid[i]) do
			self.grid[i][j]:draw()
		end
	end
end

function Level:drawSpawns()
	lg.setColor(255,255,255)
	for i,v in ipairs(self.spawns) do
		lg.rectangle("line",self.spawns[i].x,self.spawns[i].y,Level.BLOCKWIDTH,Level.BLOCKHEIGHT)
	end
end

function Level:updatePlants(dt)
	for i,v in ipairs(self.plants) do
		self.plants[i]:update(dt)
	end
end

function Level:drawPlants()
	for i,v in ipairs(self.plants) do
		self.plants[i]:draw()
	end
end









---

function Level:update(dt)
	self:updateGrid(dt)
	self:updatePlants(dt)

	self.playerHits = {}
end

function Level:draw()
	lg.push()
	lg.translate(self.tween.x,self.tween.y)
		self:drawGrid()
		self:drawSpawns()
	--	self:drawPlants()
	lg.pop()
end

return Level
