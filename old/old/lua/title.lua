local Title = Gamestate:addState('Title')

local Letter = require 'lua.letter'
local Bar = require 'lua.bar'
local Particle = require 'lua.particle'

local WIDTH = lg.getWidth()
local HEIGHT = lg.getHeight()
local CENTERX = WIDTH/2
local CENTERY = HEIGHT/2

local MAXBARS = 100
local MAXPARTICLES = 35

local titleFont = lg.newFont("res/mgsbrush.ttf",128)
local enemyFont = lg.newFont("res/squared.ttf",128)
local hudFont = lg.newFont("res/04B03.ttf",10)
local moon = lg.newImage("res/moon.png")
local bg = lg.newImage("res/bg.jpg")

local intro = true
local outro = false
local stateTimer = 0

local bars = {}
local particles = {}
local titleLetters = {}
local subtitleLetters = {}

local tweenOffset = 0 
local tweenCounter = 0
local colorTime = 0

local particlesBuffer = 0

local enemyspeak = {alpha = 0}
local debug_text = {}

local function debug(input, indent)
	if (input == nil) then
		input = "--------"
	end

	if (indent == nil) then
		indent = 0
	end

	local temp_string = tostring(input)

	for i = 1, indent, 1 do
		temp_string = "   " .. temp_string
	end

	table.insert(debug_text, temp_string)
end

local function tweenLetters()
	local tweentime = 1.0
	local overlap = 0.75

	flux.to(titleLetters[1],tweentime,{y = tweenOffset}):ease("quadinout")
	flux.to(titleLetters[2],tweentime,{y = tweenOffset}):ease("quadinout"):delay((tweentime-overlap)*1)
	flux.to(titleLetters[3],tweentime,{y = tweenOffset}):ease("quadinout"):delay((tweentime-overlap)*2)
	flux.to(titleLetters[4],tweentime,{y = tweenOffset}):ease("quadinout"):delay((tweentime-overlap)*3)
	flux.to(titleLetters[5],tweentime,{y = tweenOffset}):ease("quadinout"):delay((tweentime-overlap)*4)
	flux.to(titleLetters[6],tweentime,{y = tweenOffset}):ease("quadinout"):delay((tweentime-overlap)*5)
	flux.to(titleLetters[7],tweentime,{y = tweenOffset}):ease("quadinout"):delay((tweentime-overlap)*6)
	flux.to(titleLetters[8],tweentime,{y = tweenOffset}):ease("quadinout"):delay((tweentime-overlap)*7)
	flux.to(titleLetters[9],tweentime,{y = tweenOffset}):ease("quadinout"):delay((tweentime-overlap)*8)
	flux.to(titleLetters[10],tweentime,{y = tweenOffset}):ease("quadinout"):delay((tweentime-overlap)*9)
end

local function updateParticles(dt)
	for i,v in ipairs(particles) do
		particles[i]:update(dt)
		if particles[i]:getKill() == true then
			table.remove(particles,i)
--			i = i - 1 -- probably not needed, I don't know
		end
	end

	local interval = 0.75
	local rate = 1

	if particlesBuffer > interval then
		particlesBuffer = 0
		local offset = 20
		local y = -CENTERY - Particle.SAFEZONE
		local x = math.random(-offset-CENTERX,CENTERX-offset)
		particles[#particles + 1] = Particle:new({x = x, y = y}) 
	end
	
	particlesBuffer = particlesBuffer + rate*dt
end

local function updateTitle(dt)
	tweenCounter = tweenCounter + dt
	local tweentime = 1.0
	local overlap = 0.75
	if tweenCounter > (tweentime-overlap)*4 then
		tweenCounter = 0 	
		if tweenOffset == -120 then tweenOffset = -100 else tweenOffset = -120 end
		tweenLetters()
	end
	for i,v in ipairs(subtitleLetters) do
		subtitleLetters[i]:update(dt)
	end
end

local function drawTitle()
	love.graphics.setColor(255,255,255,255)
	lg.setFont(titleFont)
	for i,v in ipairs(titleLetters) do
		lg.print(titleLetters[i].n,titleLetters[i].x,titleLetters[i].y)
	end
end

local function drawSubtitle()
	for i,v in ipairs(subtitleLetters) do
		subtitleLetters[i]:draw()
	end
end

local function drawWipe()
	for i,v in ipairs(bars) do
		bars[i]:draw()
	end
end

local function drawParticles()
	for i,v in ipairs(particles) do
		particles[i]:draw()
	end
end

local function drawEnemyspeak()
	-- hello
	lg.setFont(enemyFont)
	lg.setColor(255,255,255,enemyspeak.alpha)
	local w = enemyFont:getWidth("enemypseak")
	local h = enemyFont:getHeight("enemypseak")
	lg.printf("Enemypseak",-w/2-40,-h/2-20,WIDTH,"center")
end

local function drawHud()
	lg.setFont(hudFont)
	lg.setColor(255,255,255)
	local paddingw = 10
	local paddingh = 5
	local w = hudFont:getWidth("Joysticks  Connected: ")
	lg.print("Joysticks Connected: "..#joysticks,-(w/2),-CENTERY+paddingh)
--[[
	local message = love.filesystem.read('.git/COMMIT_EDITMSG',75)
	w = hudFont:getWidth(message)
	h = hudFont:getHeight(message)
	lg.print(message,-(w/2),CENTERY-(5+h))

	local commit = love.filesystem.read('.git/refs/heads/develop',7)-- this is useful for release
	w = hudFont:getWidth("Commit: "..commit)
	lg.print("Commit: "..commit,-(w/2),CENTERY-(5+h+10))
--]]
	if joysticks[1] == nil then 
		lg.print("Player 1: Not Connected",-CENTERX+paddingw,-CENTERY+paddingh)
	else
		lg.print("Player 1: Connected!",-CENTERX+paddingw,-CENTERY+paddingh)
	end
	if joysticks[2] == nil then 		
	w = hudFont:getWidth("Player 2: Not Connected")
	lg.print("Player 2: Not Connected",CENTERX-paddingw-w,-CENTERY+paddingh)
	else
		w = hudFont:getWidth("Player 2: Connected!")
		lg.print("Player 2: Connected!",CENTERX-paddingw-w,-CENTERY+paddingh)
	end

	paddingh = 15
	if joysticks[3] == nil then 
		lg.print("Player 3: Not Connected",-CENTERX+paddingw,CENTERY-paddingh)
	else
		lg.print("Player 3: Connected!",-CENTERX+paddingw,CENTERY-paddingh)
	end
	if joysticks[4] == nil then 
		w = hudFont:getWidth("Player 2: Not Connected")
		lg.print("Player 4: Not Connected",CENTERX-paddingw-w,CENTERY-paddingh)
	else
		w = hudFont:getWidth("Player 2: Connected!")
		lg.print("Player 4: Connected!",CENTERX-paddingw-w,CENTERY-paddingh)
	end
end







--------

function Title:enteredState()
	numPlayers = 1

	particlesBuffer = 0
	for i = 1,MAXPARTICLES do
		particles[#particles+1] = Particle:new({x=math.random(-CENTERX,CENTERX),y= math.random(-CENTERY,CENTERY)})
	end

	local xpos = 0
	local ypos = -120
	titleLetters = 	{
					[1] = { x = xpos-305,y = ypos,n = 'F' },
					[2] = { x = xpos-230,y = ypos,n = 'a' },
					[3] = { x = xpos-160,y = ypos,n = 't' },
					[4] = { x = xpos-110,y = ypos,n = 'h' },
					[5] = { x = xpos-60,y = ypos,n = 'e' },
					[6] = { x = xpos+25,y = ypos,n = 'r' },
					[7] = { x = xpos+115,y = ypos,n = 'h' },
					[8] = { x = xpos+165,y = ypos,n = 'o' },
					[9] = { x = xpos+215,y = ypos,n = 'o' },
					[10] = { x = xpos+265,y = ypos,n = 'd' }
					}
	tweenOffset = 75 
	tweenCounter = 1

	xpos = -75
	ypos = 150
	local d = 3.5
	subtitleLetters = 	{
						[1] = Letter:new({ripple = false, lifespan = false, fadeRandom = true, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 0.05,scaleTo = 0.15, fadeSpeed = 0.1, colorR= 255,colorG = 255, colorB = 255, colorA = 0, colorToA = 255, fade= d+0.1,x = xpos-50,y=ypos, delay= d+0.1,n="p"}),
						[2] = Letter:new({ripple = false, lifespan = false, fadeRandom = true, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 0.05,scaleTo = 0.15, fadeSpeed = 0.1, colorR= 255,colorG = 255, colorB = 255, colorA = 0, colorToA = 255, fade= d+0.2,x = xpos-30,y=ypos, delay= d+0.2,n="r"}),
						[3] = Letter:new({ripple = false, lifespan = false, fadeRandom = true, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 0.05,scaleTo = 0.15, fadeSpeed = 0.1, colorR= 255,colorG = 255, colorB = 255, colorA = 0, colorToA = 255, fade= d+0.3,x = xpos-10,y=ypos, delay= d+0.3,n="e"}),
						[4] = Letter:new({ripple = false, lifespan = false, fadeRandom = true, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 0.05,scaleTo = 0.15, fadeSpeed = 0.1, colorR= 255,colorG = 255, colorB = 255, colorA = 0, colorToA = 255, fade= d+0.4,x = xpos+10,y=ypos, delay= d+0.4,n="s"}),
						[5] = Letter:new({ripple = false, lifespan = false, fadeRandom = true, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 0.05,scaleTo = 0.15, fadeSpeed = 0.1, colorR= 255,colorG = 255, colorB = 255, colorA = 0, colorToA = 255, fade= d+0.5,x = xpos+30,y=ypos, delay= d+0.5,n="s"}),
						[6] = Letter:new({ripple = false, lifespan = false, fadeRandom = true, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 0.05,scaleTo = 0.15, fadeSpeed = 0.1, colorR= 255,colorG = 255, colorB = 255, colorA = 0, colorToA = 255, fade= d+0.6,x = xpos+65,y=ypos, delay= d+0.6,n="a"}),
						[7] = Letter:new({ripple = false, lifespan = false, fadeRandom = true, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 0.05,scaleTo = 0.15, fadeSpeed = 0.1, colorR= 255,colorG = 255, colorB = 255, colorA = 0, colorToA = 255, fade= d+0.7,x = xpos+100,y=ypos, delay= d+0.7,n="b"}),
						[8] = Letter:new({ripple = false, lifespan = false, fadeRandom = true, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 0.05,scaleTo = 0.15, fadeSpeed = 0.1, colorR= 255,colorG = 255, colorB = 255, colorA = 0, colorToA = 255, fade= d+0.8,x = xpos+120,y=ypos, delay= d+0.8,n="u"}),
						[9] = Letter:new({ripple = false, lifespan = false, fadeRandom = true, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 0.05,scaleTo = 0.15, fadeSpeed = 0.1, colorR= 255,colorG = 255, colorB = 255, colorA = 0, colorToA = 255, fade= d+0.9,x = xpos+140,y=ypos, delay= d+0.9,n="t"}),
						[10] = Letter:new({ripple = false, lifespan = false, fadeRandom = true, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 0.05,scaleTo = 0.15, fadeSpeed = 0.1, colorR= 255,colorG = 255, colorB = 255, colorA = 0, colorToA = 255, fade= d+1.0,x = xpos+160,y=ypos, delay= d+1.0,n="t"}),
						[11] = Letter:new({ripple = false, lifespan = false, fadeRandom = true, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 0.05,scaleTo = 0.15, fadeSpeed = 0.1, colorR= 255,colorG = 255, colorB = 255, colorA = 0, colorToA = 255, fade= d+1.1,x = xpos+180,y=ypos, delay= d+1.1,n="o"}),
						[12] = Letter:new({ripple = false, lifespan = false, fadeRandom = true, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 0.05,scaleTo = 0.15, fadeSpeed = 0.1, colorR= 255,colorG = 255, colorB = 255, colorA = 0, colorToA = 255, fade= d+1.2,x = xpos+200,y=ypos, delay= d+1.2,n="n"})
						}
	tweenOffset = -70

	for i=1, MAXBARS do
		local w = lg.getWidth()
		local h = lg.getHeight()/MAXBARS

		bars[i] = Bar:new({width = w, height = h,x = -CENTERX, y = -CENTERY + h * (i-1) })
	end

	flux.to(enemyspeak,0.25,{alpha = 255}):ease("quadinout"):after(
			enemyspeak,0.25,{alpha = 0}):delay(1.5):ease("quadinout")
	--tweenLetters()

	intro = true
	outro = false
	stateTimer = 0
end

function Title:exitedstate()
	bar = {}
end

function Title:update(dt)
	updateTitle(dt)
	updateParticles(dt)

	if intro then
		stateTimer = stateTimer + dt
		if stateTimer > 2.5 then 
			stateTimer = 0
			intro = false
		end
	end

	if outro then
		stateTimer = stateTimer + dt
		if stateTimer > 1.5 then
			gamestate:gotoState("Game")
		end
	end
end

function Title:draw()
	lg.setColor(23,32,100)
	lg.rectangle("fill",0,0,WIDTH,HEIGHT)

	lg.setColor(255,255,255)
	love.graphics.push()
	lg.translate(CENTERX,CENTERY)
	--	lg.draw(moon,0,0,0,1,1,moon:getWidth()/2,moon:getHeight()/2)
		lg.draw(bg,0,0,0,1,1,bg:getWidth()/2,bg:getHeight()/2)
		drawParticles()
		drawTitle()
		drawSubtitle()
		drawHud()
		drawWipe()
		drawEnemyspeak()
	love.graphics.pop()

	lg.setColor(255,255,255,255)
	for index,value in pairs(debug_text) do
		love.graphics.print(value, 100, 100 + 12*index)
	end

	debug_text = {}
end

function Title:keypressed(key, unicode)
	if key == 'escape' then
		love.event.push('quit')
	elseif intro or outro then
		-- no buttons allowed
	else 
		outro = true
		bars = {}
		for i=1, MAXBARS do
			local w = lg.getWidth()
			local h = lg.getHeight()/MAXBARS
	
			bars[i] = Bar:new({width = w, reverse = true, delay = 0, height = h,x = CENTERX, y = -CENTERY + h * (i-1) })
		end
	end
end

function Title:keyreleased(key)
end

function Title:joystickpressed(joystick,button)
	if intro or outro then
		-- no buttons allowed
	else 
		outro = true
		bars = {}
		for i=1, MAXBARS do
			local w = lg.getWidth()
			local h = lg.getHeight()/MAXBARS
	
			bars[i] = Bar:new({width = w, reverse = true, delay = 0, height = h,x = CENTERX, y = -CENTERY + h * (i-1) })
		end
	end
end

function Title:mousepressed(x, y, button)
end

function Title:mousereleased(x, y, button)
end
