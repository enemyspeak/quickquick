
local Player = class('Player')

Player.static.BULLET = lg.newImage('res/leaf.png')

Player.static.GRAVITY = 200
Player.static.MAXYSPEED = 1200
Player.static.BLOCKWIDTH = 16
Player.static.BLOCKHEIGHT = 16

Player.static.RELOADTIME = 0.25
Player.static.MAXBULLETS = 6
Player.static.BULLETREGEN = 0.75

function Player:initialize(attributes)
	attributes = attributes or {}
	
	self.x = attributes.x or 1
	self.y = attributes.y or 1

	self.width = 16
	self.height = 32

	self.direction = 1
	self.state = 0
	self.animationCounter = 1
	self.animationSpeed = 0.5
	self.animationTimer = math.random(5,20)/10

	self.reload = 0
	self.warpCooldown = 0

	self.playerNum = attributes.playerNum or 1

	self.normalAcceleration = 200
	self.dragActive = 0.9
	self.dragPassive = 0.7
	self.maxSpeed = 200 --800
	self.maxSpeed_sq = self.maxSpeed * self.maxSpeed

	self.jumpTime = 0.1
	self.maxJump = 0.1
	self.jumpHeight = 64*18 --64*3 -- 64 is a tile

	self.xVel = 0
	self.yVel = 0
	
	self.joystick = attributes.joystick or nil

	self.input = 	{
					left = false,
					right = false,
					up = false,
					down = false,
					jump = false,
					fire = false,
					warp = false
					}

	self.color = 	{
					r = 255,
					g = 255,
					b = 255,
					a = 255,
					}

	self.isTransitioning = false

	self.collisions = {}

	self.numBullets = 0
	self.bulletTick = 0
	self.bulletOffset = 0
	flux.to(self,1,{numBullets = Player.MAXBULLETS})

	self.fancyCollisions = 	{
							top = false,
							right = false,
							bottom = false,
							left = false
							}
end

function Player:getPosition()
	return self.x,self.y
end

function Player:setPosition(x,y)
	self.x = x
	self.y = y
end	

function Player:setTransitioning(v,dir)
	self.isTransitioning = v
	local t = 0.4
	if self.isTransitioning then
		if dir == 3 then
			flux.to(self,t,{y = 80}):ease("quadinout")
		end
		if dir == 1 then
			flux.to(self,t,{y = 520}):ease("quadinout")
		end
		if self.playerNum == 1 then
			if dir == 2 then
				flux.to(self,t,{x = 70}):ease("quadinout")
			end
			if dir == 4 then
				flux.to(self,t,{x = 540}):ease("quadinout")
			end
		else
			if dir == 2 then
				flux.to(self,t,{x = 640+70}):ease("quadinout")
			end
			if dir == 4 then
				flux.to(self,t,{x = 640+540}):ease("quadinout")
			end
		end
	end
end

function Player:getTransitioning()
	return self.isTransitioning
end

function Player:updateMovement( dt )
	local function magnitude_2d(x, y)
		return math.sqrt(x*x + y*y)
	end

	local function round(num, idp)
		local mult = 10^(idp or 0)
		return math.floor(num * mult + 0.5) / mult
	end

	local function magnitude_2d_sq(x, y)
		return x*x + y*y
	end

	local function normalize_2d(x, y)
		local mag = magnitude_2d(x, y)
		if mag == 0 then return {0,0} end
		return {x/mag, y/mag}
	end

	local tempXAccel = 0
	local tempYAccel = 0
	
	if (self.input.left and (not self.input.right)) then 
		tempXAccel = -1
	elseif (self.input.right and (not self.input.left)) then 
		tempXAccel = 1 
	end

	local tempNormAccel = normalize_2d(tempXAccel,tempYAccel)
	tempXAccel = tempNormAccel[1]*self.normalAcceleration

	local tempXVel = self.xVel
	local tempYVel = 0
	local curSpeed = magnitude_2d(self.xVel,0)
	
	if ((self.normalAcceleration + curSpeed) > self.maxSpeed) then
		local accelMagnitude = self.maxSpeed - curSpeed
		if (accelMagnitude < 0) then accelMagnitude = 0 end

		tempXAccel = tempNormAccel[1]*accelMagnitude
		--tempYAccel = tempNormAccel[2]*accelMagnitude
	end

	tempXVel = tempXVel + tempXAccel
	--tempYVel = tempYVel + tempYAccel

	local temp_vel = magnitude_2d_sq(tempXVel, 0)

	if(math.abs(temp_vel) > self.maxSpeed_sq) then
		tempXVel = self.xVel
	--	tempYVel = self.yVel
	end

	local temp_drag = self.dragPassive

	if (self.input.x_has_input or self.input.y_has_input) then
		temp_drag = self.dragActive
	end

	self.xVel = tempXVel * temp_drag

	if self.jumpTime > 0 and self.input.jump then	-- jump!
		self.jumpTime = self.jumpTime - dt
		self.yVel = self.yVel - self.jumpHeight * dt
	end

	if self.fancyCollisions.left and self.input.jump then
		self.jumpTime = self.maxJump
		self.xVel = 100
		self.yVel = 0
	elseif self.fancyCollisions.right and self.input.jump then
		self.jumpTime = self.maxJump
		self.yVel = 0
		self.xVel = -100
	end

	if self.fancyCollisions.top then
		self.jumpTime = 0
		self.yVel = 0
	end
		
	self.y = self.y + self.yVel*dt
	self.yVel = self.yVel + Player.GRAVITY * dt
	
	if self.yVel > Player.MAXYSPEED then
		self.yVel = Player.MAXYSPEED
	end
	
	if self.fancyCollisions.bottom then
		self.jumpTime = self.maxJump
		self.yVel = 0
	end
end

function Player:setCollision(v,c)
	self.collisions[v] = c
end

function Player:move(dt)
	self.x = self.x + self.xVel*dt
	self.y = self.y + self.yVel*dt
end

function Player:shoot()
	self.reload = Player.RELOADTIME
	self.numBullets = self.numBullets - 1
	self.bulletTick = 0
	--animation hooks here
end

function Player:drawCollision()
	lg.setColor(0,0,0)
	lg.rectangle("fill",self.x+35,self.y-65,150,55)
	lg.setColor(0,255,255)
	lg.print("self.yVel: "..self.yVel, self.x+40,self.y-60)
	lg.print("bottom: "..tostring(self.fancyCollisions.bottom), self.x+40,self.y-40)
	lg.print("top: "..tostring(self.fancyCollisions.top), self.x+40,self.y-30)
end

function Player:drawDebug()
	lg.setColor(0,0,0)
	lg.rectangle("fill",self.x-5,self.y-105,150,45)
	lg.setColor(255,255,255)
	lg.print( math.floor(self.x/16).." ".. math.floor(self.y/16), self.x-5,self.y-100)
	lg.print( "yYel: "..self.yVel, self.x-5,self.y-90)
	lg.print( "xYel: "..self.xVel, self.x-5,self.y-80)
end

function Player:updateState()
	if (self.input.left and (not self.input.right)) then 
		self.state = 4 -- left
	elseif (self.input.right and (not self.input.left)) then 
		self.state = 2 -- right
	else
		self.state = 0 -- standing
	end

	if math.abs(self.yVel) < 100 then else
		self.state = 3
	end
end

function Player:updateBullets(dt)
	self.bulletOffset = self.bulletOffset + dt
end

function Player:drawBullets()
	for i = 1,self.numBullets do
		---[[
		lg.draw(Player.BULLET,self.x,self.y,
								self.bulletOffset + math.rad((360/self.numBullets)*i),
								1,1,
								Player.BULLET:getHeight()/2+30,
								Player.BULLET:getWidth()/2)
		--]]
	end
end

function Player:updateAnimation(dt)
	self.animationTimer = self.animationTimer + dt

--[[
	if self.animationTimer > self.animationSpeed then
		self.animationTimer = 0
		self.animationCounter = self.animationCounter + 1
	end
	]]
end

function Player:setLeftCollision(v)
	self.fancyCollisions.left = v
end

function Player:setRightCollision(v)
	self.fancyCollisions.right = v
end

function Player:setTopCollision(v)
	self.fancyCollisions.top = v
end

function Player:setBottomCollision(v)
	self.fancyCollisions.bottom = v
end

function Player:update(dt)
	self:updateMovement(dt)
	--self:updateCollision()
	self.reload = self.reload - dt
	self.bulletTick = self.bulletTick + dt
	self.warpCooldown = self.warpCooldown - dt
	if self.bulletTick > Player.BULLETREGEN then
		if self.numBullets >= Player.MAXBULLETS then
			-- do nothing.
			self.bulletTick = 0
		else
			self.numBullets = self.numBullets + 1
			self.bulletTick = 0
		end
	end

	self:move(dt)
	self:updateState()
	self:updateAnimation(dt)
	self:updateBullets(dt)

	self:setBottomCollision(false)
	self:setTopCollision(false)
	self:setLeftCollision(false)
	self:setRightCollision(false)
end

function Player:draw()
	lg.setColor(self.color.r,self.color.g,self.color.b,self.color.a)
	lg.setLineStyle("smooth")
	lg.rectangle("fill",self.x,self.y-self.height/2,self.width,self.height)
	lg.setColor(255,255,255)
	self:drawBullets()
end

function Player:keypressed(key)
	local function op_xor(a, b)
		return ((a or b) and (not (a and b)))
	end

	self.input.x_has_input = false
	self.input.y_has_input = false
	self.input.left = false
	self.input.right = false
	self.input.up = false
	self.input.down = false
	
	self.input.jump = false
	self.input.fire = false
	self.input.warp = false

    self.input.left = love.keyboard.isDown("left") or love.keyboard.isDown("a")
    self.input.right = love.keyboard.isDown("right") or love.keyboard.isDown("d")
    self.input.up = love.keyboard.isDown("up") or love.keyboard.isDown("w")
    self.input.down = love.keyboard.isDown("down") or love.keyboard.isDown("s")

    self.input.jump = love.keyboard.isDown(" ") 
    self.input.fire = love.keyboard.isDown("q") 
    self.input.warp = love.keyboard.isDown("e") 

	if self.joystick == nil then else
		if self.joystick:isConnected() then
			local deadzone = 0.2
			local stick = 	{
							up = false,
							right = false,
							down = false,
							left = false
							}

			if self.joystick:getGamepadAxis("leftx") < deadzone then
				stick.left = true
			end
			if self.joystick:getGamepadAxis("leftx") > -deadzone then
				stick.right = true
			end
			if self.joystick:getGamepadAxis("lefty") < deadzone then
				stick.up = true -- this is a stickup
			end
			self.input.left = self.input.left
			if self.joystick:getGamepadAxis("lefty") > -deadzone then
				stick.down = true
			end

			self.input.up = op_xor(self.joystick:isGamepadDown("dpdown"),stick.up) 
			self.input.left = op_xor(self.joystick:isGamepadDown("dpright"),stick.left)
			self.input.right = op_xor(self.joystick:isGamepadDown("dpleft"),stick.right)
			self.input.down = op_xor(self.joystick:isGamepadDown("dpup"),stick.down)

			self.input.jump = self.input.jump or self.joystick:isGamepadDown("a")
			self.input.fire = self.input.fire or self.joystick:isGamepadDown("x")
		else
			self.color = {r = 255,g = 0,b = 0}
		end
	end

	self.input.x_has_input = op_xor(self.input.right, self.input.left)
	self.input.y_has_input = op_xor(self.input.up, self.input.down)

	if self.input.y_has_input then
		if self.input.up then 
			self.direction = 1
		elseif self.input.down then
			self.direction = 3
		end
	elseif self.input.x_has_input then
		if self.input.left then
			self.direction = 4
		elseif self.input.right then
			self.direction = 2
		end
	end
end

return Player
