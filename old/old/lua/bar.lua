local Bar = class('Bar')

Bar.static.CENTERX = love.graphics.getWidth()/2
Bar.static.CENTERY = love.graphics.getHeight()/2

function Bar:initialize(attributes)
	self.x = attributes.x or 0
	self.y = attributes.y or 0

	self.width = attributes.width or 16
	self.height = attributes.height or 16

	self.reverse = attributes.reverse or false

	local tweenlength = 0.5
	self.tweentime = attributes.t or 1- math.pow(math.random()*tweenlength-1,2)
	self.delay = attributes.delay or 2

	if self.reverse then
		flux.to(self,0.5+self.tweentime/2,{x = -lg.getWidth()/2}):ease("sinein"):delay(self.delay)
	else
		flux.to(self,0.5+self.tweentime/2,{width = 0}):ease("sinein"):delay(self.delay)
	end
end

function Bar:draw()
	lg.setColor(0,0,0)
	lg.rectangle("fill",self.x,self.y,self.width,self.height)
end

return Bar
