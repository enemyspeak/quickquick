
local DeadPlayer = class('DeadPlayer')

DeadPlayer.static.BULLET = lg.newImage('res/leaf.png')

DeadPlayer.static.GRAVITY = 200
DeadPlayer.static.MAXYSPEED = 1200
DeadPlayer.static.BLOCKWIDTH = 16
DeadPlayer.static.BLOCKHEIGHT = 16

function DeadPlayer:initialize(attributes)
	attributes = attributes or {}
	
	self.x = attributes.x or 1
	self.y = attributes.y or 1

	self.width = 16
	self.height = 32

	self.direction = 1

	self.animationCounter = 1
	self.animationSpeed = 0.5
	self.animationTimer = math.random(5,20)/10

	self.DeadPlayerNum = attributes.DeadPlayerNum or 1

	self.normalAcceleration = 200
	self.dragActive = 0.9
	self.dragPassive = 0.7
	self.maxSpeed = 200 --800
	self.maxSpeed_sq = self.maxSpeed * self.maxSpeed

	self.xVel = attributes.xVel or 0
	self.yVel = attributes.xVel or 0
	
	self.isTransitioning = false
	self.numBullets = attributes.numBullets or 0

	self.input = 	{
					left = false,
					right = false,
					up = false,
					down = false,
					jump = false,
					fire = false,
					warp = false
					}

	self.color = 	{
					r = 255,
					g = 255,
					b = 255,
					a = 255,
					}
	self.collisions = {}
	self.fancyCollisions = 	{
							top = false,
							right = false,
							bottom = false,
							left = false
							}
end

function DeadPlayer:getPosition()
	return self.x,self.y
end

function DeadPlayer:setPosition(x,y)
	self.x = x
	self.y = y
end	

function DeadPlayer:setTransitioning(v,dir)
	self.isTransitioning = v
	local t = 0.4
	if self.isTransitioning then
		if dir == 3 then
			flux.to(self,t,{y = 80}):ease("quadinout")
		end
		if dir == 1 then
			flux.to(self,t,{y = 520}):ease("quadinout")
		end
		if self.DeadPlayerNum == 1 then
			if dir == 2 then
				flux.to(self,t,{x = 70}):ease("quadinout")
			end
			if dir == 4 then
				flux.to(self,t,{x = 540}):ease("quadinout")
			end
		else
			if dir == 2 then
				flux.to(self,t,{x = 640+70}):ease("quadinout")
			end
			if dir == 4 then
				flux.to(self,t,{x = 640+540}):ease("quadinout")
			end
		end
	end
end

function DeadPlayer:getTransitioning()
	return self.isTransitioning
end

function DeadPlayer:updateMovement( dt )
	local function magnitude_2d(x, y)
		return math.sqrt(x*x + y*y)
	end

	local function round(num, idp)
		local mult = 10^(idp or 0)
		return math.floor(num * mult + 0.5) / mult
	end

	local function magnitude_2d_sq(x, y)
		return x*x + y*y
	end

	local function normalize_2d(x, y)
		local mag = magnitude_2d(x, y)
		if mag == 0 then return {0,0} end
		return {x/mag, y/mag}
	end

	local tempXAccel = 0
	local tempYAccel = 0
	
	if (self.input.left and (not self.input.right)) then 
		tempXAccel = -1
	elseif (self.input.right and (not self.input.left)) then 
		tempXAccel = 1 
	end

	local tempNormAccel = normalize_2d(tempXAccel,tempYAccel)
	tempXAccel = tempNormAccel[1]*self.normalAcceleration

	local tempXVel = self.xVel
	local tempYVel = 0
	local curSpeed = magnitude_2d(self.xVel,0)
	
	if ((self.normalAcceleration + curSpeed) > self.maxSpeed) then
		local accelMagnitude = self.maxSpeed - curSpeed
		if (accelMagnitude < 0) then accelMagnitude = 0 end

		tempXAccel = tempNormAccel[1]*accelMagnitude
		--tempYAccel = tempNormAccel[2]*accelMagnitude
	end

	tempXVel = tempXVel + tempXAccel
	--tempYVel = tempYVel + tempYAccel

	local temp_vel = magnitude_2d_sq(tempXVel, 0)

	if(math.abs(temp_vel) > self.maxSpeed_sq) then
		tempXVel = self.xVel
	--	tempYVel = self.yVel
	end

	local temp_drag = self.dragPassive

	if (self.input.x_has_input or self.input.y_has_input) then
		temp_drag = self.dragActive
	end

	self.xVel = tempXVel * temp_drag
	
	self.y = self.y + self.yVel*dt
	self.yVel = self.yVel + DeadPlayer.GRAVITY * dt
	
	if self.yVel > DeadPlayer.MAXYSPEED then
		self.yVel = DeadPlayer.MAXYSPEED
	end
end

function DeadPlayer:setCollision(v,c)
	self.collisions[v] = c
end

function DeadPlayer:move(dt)
	self.x = self.x + self.xVel*dt
	self.y = self.y + self.yVel*dt
end

function DeadPlayer:updateState()
	if (self.input.left and (not self.input.right)) then 
		self.state = 4 -- left
	elseif (self.input.right and (not self.input.left)) then 
		self.state = 2 -- right
	else
		self.state = 0 -- standing
	end

	if math.abs(self.yVel) < 100 then else
		self.state = 3
	end
end

function DeadPlayer:setBottomCollision(v)
	self.collisions.bottom = v
end

function DeadPlayer:updateAnimation(dt)
	self.animationTimer = self.animationTimer + dt

--[[
	if self.animationTimer > self.animationSpeed then
		self.animationTimer = 0
		self.animationCounter = self.animationCounter + 1
	end
	]]
end

function DeadPlayer:update(dt)
	self:updateMovement(dt)
	self:move(dt)
	self:updateState()
	self:updateAnimation(dt)
end

function DeadPlayer:draw()
	lg.setColor(self.color.r,self.color.g,self.color.b,self.color.a)
	lg.setColor(128,128,128)

	lg.setLineStyle("smooth")
	lg.rectangle("fill",self.x,self.y-self.height/2,self.width,self.height)
end

return DeadPlayer
