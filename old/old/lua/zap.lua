local Zap = Gamestate:addState('Zap')

local Letter = require 'lua.letter'

local WIDTH = lg.getWidth()
local HEIGHT = lg.getHeight()
local CENTERX = WIDTH/2
local CENTERY = HEIGHT/2

local debug_text = {}

local frameCounter = 1

local function debug(input, indent)
	if (input == nil) then
		input = "--------"
	end

	if (indent == nil) then
		indent = 0
	end

	local temp_string = tostring(input)

	for i = 1, indent, 1 do
		temp_string = "   " .. temp_string
	end

	table.insert(debug_text, temp_string);
end

local function updateZap(dt)
	frameCounter = frameCounter + 1
	if frameCounter > 10 then 
		gamestate:gotoState("Game")
	end
end

local function drawZap()
	lg.setColor(255,255,255)
	lg.rectangle("fill",-CENTERX,-CENTERY,WIDTH,HEIGHT)
end

local function drawSubZap()
end







--------

function Zap:enteredState()
end

function Zap:exitedstate()
end

function Zap:update(dt)
	updateZap(dt)
end

function Zap:draw()
	lg.setColor(15,16,46)
	lg.rectangle("fill",0,0,WIDTH,HEIGHT)

	lg.setColor(255,255,255)
	love.graphics.push()
	lg.translate(CENTERX,CENTERY)
		drawZap()
		drawSubZap()
	love.graphics.pop()

	lg.setColor(255,255,255,255)
	for index,value in pairs(debug_text) do
		love.graphics.print(value, 100, 100 + 12*index)
	end

	debug_text = {}
end

function Zap:keypressed(key, unicode)
	if key == 'escape' then
		love.event.push('quit')
	else 
	    gamestate:gotoState("Zap")
	end
end

function Zap:keyreleased(key)
end

function Zap:gamepadpressed(joystick,button)
end

function Zap:mousepressed(x, y, button)
end

function Zap:mousereleased(x, y, button)
end
