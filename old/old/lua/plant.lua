		
local Plants = class('Plants')

function Plants:initialize(attributes)
	local attributes = attributes or {}
	self.offsetX = attributes.offsetX or 0
	self.offsetY = attributes.offsetY or 0

	self.x = attributes.x or 1
	self.y = attributes.y or 1
	self.width = 16
	self.height = 16

	self.n = attributes.n or 15
	self.scaleX = math.random(0,math.pi*2)
	self.scaleY = math.random(0,math.pi*2)
end

function Plants:update(dt)	
	self.scaleX = self.scaleX + dt*2
	self.scaleY = self.scaleY + dt*2
end

function Plants:draw()
	local sx = (math.sin(self.scaleX) + 1)
	local sy = (math.sin(self.scaleY) + 1)
	lg.setColor(255,255,255,255)
	lg.rectangle("fill",self.offsetX + self.x*self.width-(sx/2),self.offsetY + self.y*self.height-(sy/2),self.width+sx,self.height+sy)
end

return Plants
