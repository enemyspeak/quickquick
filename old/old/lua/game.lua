
local Game = Gamestate:addState('Game')

local Player =  require 'lua.player'
local DeadPlayer = require 'lua.deadplayer'
local Level = require 'lua.level'
local Bar = require 'lua.bar'
local Bullet = require('lua.bullet')
local DeadBullet = require 'lua.deadbullet'

local bg = lg.newImage("res/bg2.jpg")

local score
local levels = {}
local players = {}
local deadplayers = {}
local bullets = {}
local deadbullets = {}
local warps = {}
local input = {}
local bars = {}

local intro = true
local stateTimer = 0
local accumulator = 0

local MAXBARS = 50
local MAXPLAYERS = 4

local function createPlayer(n)
	local px, py
	local temp = true
	while temp do
--[[	px = math.random(2,lg.getWidth()-2) 	-- old, just straight randomization for spawns
		py = math.random(2,lg.getHeight()-2)
		if levels[1]:checkGrid(px,py) == true then
			temp = false
		end 	
--]]
		local maxDistance = 20 -- beyond this, assume you're good
		local d = maxDistance
		px,py = levels[1]:getSpawnPoint()
	-- 	get distance to players (recursive?)
	--	reject if it's too close so there isn't spawn camping
		temp = false
	end
	
	players[n or #players+1] = Player:new({x = px,y = py,joystick = joysticks[n or #players + 1]})
	levels[1]:addPlayer(n or #players+1,px,py)
end

local function updatePlayers(dt)
	for i,v in ipairs(deadplayers) do
		deadplayers[i]:update(dt)
		local px,py = deadplayers[i]:getPosition()
		local x,y = levels[1]:updateDeadPlayer(i,px,py)
		if py > y then deadplayers[i]:setBottomCollision(true) end
		deadplayers[i]:setPosition(x,y)
	end

	for i,v in ipairs(players) do
		if players[i] == false then 
		else
			-- this probably shouldn't be in game
			if players[i].input.warp and players[i].warpCooldown < 0 then
				local d = players[i].direction 
				local px,py = players[i]:getPosition()
				local warpFactor = 6 * 16
				if d == 1 then				
					py = py - warpFactor 
				elseif d == 2 then
					px = px + warpFactor
				elseif d == 3 then
					py = py + warpFactor
				elseif d == 4 then
					px = px - warpFactor
				end
				py = py - 10

				levels[1]:warpPlayer(i,px,py)
				local x,y = players[i]:getPosition()
				warps[#warps+1] = {x = x,y = y}
				warps[#warps+1] = {x = px,y = py}

				players[i]:setPosition(px,py)
				players[i].warpCooldown = 1
				players[i].numBullets = 0
			end

			players[i]:update(dt)
			local px,py = players[i]:getPosition()

			local x,y = levels[1]:updatePlayer(i,px,py)
			if py > y then players[i]:setBottomCollision(true) end
			if py < y then players[i]:setTopCollision(true) end
			if px > x then players[i]:setRightCollision(true) end
			if px < x then players[i]:setLeftCollision(true) end
			
			players[i]:setPosition(x,y)
		end
	end
	if #levels[1].playerHits ~= 0 then
		for i,v in ipairs(levels[1].playerHits) do
			local n = v.n
			local owner = v.owner

			if n == owner then else
				local px,py = players[n]:getPosition()
				local dir = players[n].direction
				local xv = players[n].xVel or 0
				local yv = players[n].yVel or 0
				local m = players[n].numBullets
				levels[1]:addDeadPlayer(#deadplayers+1,px,py)
				levels[1]:removePlayer(n)
				players[n] = false 				-- died here rip
				deadplayers[#deadplayers+1] = DeadPlayer:new({x = px, y= py, xVel = xv, yVel = yv, numBullets = m, direction = dir})
			end
		end
	end
end

local function updateBullets(dt)
	for i,v in ipairs(players) do
		if players[i] == false then

		elseif players[i].reload <= 0 and players[i].numBullets > 0 and players[i].input.fire then
			local dir = players[i].direction
			local px,py = players[i]:getPosition()
			local offset = 3
			if dir == 1 then
				py = py - (players[i].height/2+offset)
				px = px + (players[i].width/2)
			elseif dir == 2 then
				px = px + (players[i].width/2+offset)
				py = py + players[i].height/2
			elseif dir == 3 then
				py = py + (players[i].height/2+offset)
			elseif dir == 4 then
				px = px - (players[i].width/2+offset)
				py = py + players[i].height/2
			end
			local temp = false
			for j,k in ipairs(bullets) do
				if bullets[j] == true then
					temp = j
					break
				end
			end
			local n
			if temp == false then
				bullets[#bullets + 1] = Bullet:new({ x = px, y = py,owner = i, direction = dir or 1})
				n = #bullets
			else
				n = temp
				bullets[temp] = Bullet:new({ x = px, y = py,owner = i,direction = dir or 1})
			end

			players[i]:shoot()
			levels[1]:addBullet(n,px,py,i)
		end
	end

	for i,v in ipairs(bullets) do
		if bullets[i] == true then
			-- NOT_A_BULLET
		else
			bullets[i]:update(dt)
			local bx,by = bullets[i]:getPosition()
			local temp = levels[1]:updateBullet(i,bx,by)
			if temp then
				levels[1]:removeBullet(i)
				bullets[i] = true
			end
		end
	end
end

local function drawBullets()
	for i,v in ipairs(bullets) do
		if bullets[i] == true then
			-- NOT_A_BULLET
		else
			bullets[i]:draw()
		end
	end
end

local function drawPlayers()
	for i,v in ipairs(players) do
		if players[i] == false then else
			players[i]:draw()
			players[i]:keypressed(key)
		end
	end
	for i,v in ipairs(deadplayers) do
		deadplayers[i]:draw()
	end
end

local function drawMouse()
	lg.setColor(255,255,255,128)
	local mx,my = love.mouse.getPosition()
	lg.print(math.floor(mx/16)..", "..math.floor(my/16),10,100)
	lg.rectangle("fill",math.floor(mx/16)*16,math.floor(my/16)*16,16,16)
end

local function updateLevels(dt)
	for i,v in ipairs(levels) do
		levels[i]:update(dt)
		if levels[i]:getFinished() then
			table.remove(levels,1)
			break
		end
	end
end

local function drawLevels()
	for i,v in ipairs(levels) do
		levels[i]:draw()
	end
end

local function drawWipe()
	for i,v in ipairs(bars) do
		bars[i]:draw()
	end
end

local function drawWarps()
	for i,v in ipairs(warps) do
		if i == 1 then else
			if i % 2 == 0 then
				lg.setColor(255,255,255)					
				lg.line(warps[i].x,warps[i].y,warps[i-1].x,warps[i-1].y)
			end
		end
	end
end

local function scrollLevel(dir)
	for i,v in ipairs(levels) do
		levels[i]:scrollLevel(dir)
	end

	table.insert(levels,Level:new({width = 48, height = 34,scroll = dir}))	
end

local function debugCollision()
	local mx,my = love.mouse.getPosition()
	players[1]:setPosition(mx,my)
end









--------------

function Game:enteredState()
	accumulator = 0
	local numPlayers = 1
	score = 0

	intro = true
	stateTimer = 0

	bullets = {}
	deadbullets = {}
	warps = {}
	
	levels = {}
	levels[#levels + 1] = Level:new({width = 48, height = 34, scroll = 0})

	players = {}
	deadplayers = {}
	for i = 1, MAXPLAYERS do
		players[i] = false
	end

	createPlayer(1)
	createPlayer(2)

	bars = {}
	for i=1, MAXBARS do
		local w = lg.getWidth()
		local h = lg.getHeight()/MAXBARS

		bars[i] = Bar:new({width = w, height = h,x = 0, y = 0 + h * (i-1), delay = 0})
	end
end

function Game:exitedState()
    hs:add("a", gameScore)
end

function Game:fixedUpdate(dt)
	updatePlayers(dt)
	updateBullets(dt)	
end

function Game:update(dt)
	if intro then
		stateTimer = stateTimer + dt
		if stateTimer > 1 then
			bars = {}
			intro = false
		end
	else
		accumulator = accumulator + dt
		local timestep = 0.02
		if accumulator > timestep then
			while accumulator >= 0.02 do
				accumulator = accumulator - 0.02
				self:fixedUpdate(dt)
			end
		end

		updateLevels(dt)
	end
end

function Game:draw()
	lg.setColor(170,170,170)
	lg.rectangle("fill",0,0,lg.getWidth(),lg.getHeight())
	lg.setColor(255,255,255)
	--lg.draw(bg,lg.getWidth()/2,lg.getHeight()/2,0,1,1,bg:getWidth()/2,bg:getHeight()/2)
	drawLevels()
	drawPlayers()
	drawWarps()
	drawBullets()
	drawWipe()
end

function Game:keypressed(key, unicode)
	if key == 'n' then 
		Game:enteredState()
	end
--[[
	if key == 'w' then
		scrollLevel(1)
	elseif key == 'a' then
		scrollLevel(4)
	elseif key == 'd' then
		scrollLevel(2)
	elseif key == 's' then
		scrollLevel(3)
	end
	--]]
end

function Game:joystickpressed(joystick, button)
	for i=1,#joysticks do
        if joysticks[i] == joystick then
        	if players[i] == false then
        		createPlayer(i)
        	end
        end
    end
end
