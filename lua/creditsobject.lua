local CreditsObject = class('CreditsObject')

CreditsObject.static.LETTER = require 'lua.letter'
CreditsObject.static.SPEED = 0.125

function CreditsObject:initialize(attributes)
	self.x = attributes.x or 0
	self.y = attributes.y or 0

	self.label = attributes.label or "nolabel" -- title "difficulty"
	self.labelLetters = {}	-- letters in title
	self.options = attributes.options or false -- option names
	self.optionLetters = {}

	self.value = attributes.value or 1 -- this stores what this ting is set as

	self.toggle = attributes.toggle or false
	self.button = attributes.button or false
	self.highlight = attributes.highlight or false
	self.func = attributes.func or false

	if self.highlight then
		self.color = { r = 255, g = 255, b = 255}
	else
		self.color = { r = 124, g = 124, b = 124}
	end

	self.fadeDelay = attributes.fadeDelay or 0.75
	self.interval = self.fadeDelay or 0
	local letterSpacing = 45
	local indent = -(self.label:len()*letterSpacing)/2
	for i = 1, self.label:len() do
	   	self.labelLetters[i] = CreditsObject.LETTER:new({x = self.x + indent + letterSpacing * (i-1), y = self.y,  scale = 1, scaleTo = 0.34, fade = self.fadeDelay + CreditsObject.SPEED*i, delay = self.fadeDelay + CreditsObject.SPEED*i, fadeSpeed = CreditsObject.SPEED, colorR = 255, colorG = 255, colorB = 255, colorA = 0, colorToA = 255, n = self.label:sub(i,i)})
	end
	if self.options == false then else
		self.optionLetters = {}
		letterSpacing = 40
		local offsetx = -(letterSpacing * #self.options[self.value]/2)
		local offsety = 40
		for i = 1, #self.options[self.value] do
    		self.optionLetters[i] = CreditsObject.LETTER:new({ripple="false",amp=3,x = self.x + offsetx + letterSpacing * (i-1), y = self.y+offsety,  scale = 0.1, scaleTo = 0.25, fade = self.fadeDelay + CreditsObject.SPEED*(#self.labelLetters + i), delay = self.fadeDelay + CreditsObject.SPEED*(#self.labelLetters + i), fadeSpeed = CreditsObject.SPEED, colorR = 255, colorG = 255, colorB = 255, colorA = 0, colorToA = 255, n = self.options[self.value]:sub(i,i)})	
		end
	end
end

function CreditsObject:getDelay()
	return self.fadeDelay + CreditsObject.SPEED*(#self.labelLetters + #self.optionLetters)
end

function CreditsObject:getValue()
	return self.value
end

function CreditsObject:incrementValue(v)
	if self.value == false or self.button then else
		self.value = self.value + v
		if self.value < 1 then self.value = #self.options end
		if self.value > #self.options then self.value = 1 end
		self:createOption() -- refresh option
	end
end

function CreditsObject:select()
	if self.toggle then
		self:incrementValue(1)
	elseif self.button then
		if self.func == false then else
			self.func()
		end
	end
end

function CreditsObject:setHighlight(v)
	self.highlight = v
	if self.highlight then
		self.color = { r = 255, g = 255, b = 255}
	else
		self.color = { r = 124, g = 124, b = 124}
	end
end

function CreditsObject:setLevelComplete(v)
	for i,v in ipairs(self.labelLetters) do
		self.labelLetters[i]:startFade(0,1)
	end
	for i,v in ipairs(self.optionLetters) do
		self.optionLetters[i]:startFade(0,1)
	end
end

function CreditsObject:update(dt) 	-- CreditsObjects should color flash?
	self.interval = self.interval + dt
	local speed = 2
	if self.interval > speed then
		self.interval = 0
		local function HSL(h, s, l, a)
		    if s<=0 then return l,l,l,a end
		    h, s, l = h/256*6, s/255, l/255
		    local c = (1-math.abs(2*l-1))*s
		    local x = (1-math.abs(h%2-1))*c
		    local m,r,g,b = (l-.5*c), 0,0,0
		    -- i wish i was a cosmic shit-kicker like @jakevsrobots
		    if h < 1     then r,g,b = c,x,0
		    elseif h < 2 then r,g,b = x,c,0
		    elseif h < 3 then r,g,b = 0,c,x
		    elseif h < 4 then r,g,b = 0,x,c
		    elseif h < 5 then r,g,b = x,0,c
		    else              r,g,b = c,0,x
		    end return (r+m)*255,(g+m)*255,(b+m)*255,a
		end
		local colorScale = 8
		local temp = true
		local r,g,b,a
		while temp do
			temp = false
			local d = 1*colorScale
			local h = d + math.random()*math.random(100,500)
			r,g,b,a = HSL(h,110,108,255)
			if r > 255 then r = 255 end
			if g > 255 then g = 255 end
			if b > 255 then b = 255 end
			local mincolor = 128
			if r * g * b / 3 < mincolor then
				temp = true
			end
		end

		flux.to(self.color,speed,{r =r,g=g,b=b}):ease("quadinout")
	end
	for i,v in ipairs(self.labelLetters) do
		self.labelLetters[i]:setColor(self.color.r,self.color.g,self.color.b,self.color.a)
		self.labelLetters[i]:update(dt)
	end
	for i,v in ipairs(self.optionLetters) do
		self.optionLetters[i]:setColor(self.color.r,self.color.g,self.color.b,self.color.a)
		self.optionLetters[i]:update(dt)
	end
end

function CreditsObject:draw()
	for i,v in ipairs(self.labelLetters) do
		self.labelLetters[i]:draw()
	end
	for i,v in ipairs(self.optionLetters) do
		self.optionLetters[i]:draw()
	end
end

return CreditsObject
