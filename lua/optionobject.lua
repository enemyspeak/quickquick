local OptionObject = class('OptionObject')

OptionObject.static.LETTER = require 'lua.letter'
OptionObject.static.SPEED = 0.025

function OptionObject:initialize(attributes)
	self.x = attributes.x or 0
	self.y = attributes.y or 0

	self.label = attributes.label or "nolabel" -- title "difficulty"
	self.labelLetters = {}	-- letters in title
	self.options = attributes.options or false -- option names
	self.optionLetters = {}

	self.value = attributes.value or 1 -- this stores what this ting is set as

	self.toggle = attributes.toggle or false
	self.button = attributes.button or false
	self.highlight = attributes.highlight or false
	self.func = attributes.func or false

	if self.highlight then
		self.color = { r = 255, g = 255, b = 255}
	else
		self.color = { r = 124, g = 124, b = 124}
	end

	self.fadeDelay = attributes.fadeDelay or 0.75
	local letterSpacing = 40
	local indent = 30
	for i = 1, self.label:len() do
	   	self.labelLetters[i] = OptionObject.LETTER:new({x = self.x + indent + letterSpacing * (i-1), y = self.y,  scale = 0.75, scaleTo = 0.25, fade = self.fadeDelay + OptionObject.SPEED*i, delay = self.fadeDelay + OptionObject.SPEED*i, fadeSpeed = OptionObject.SPEED, colorR = 255, colorG = 255, colorB = 255, colorA = 0, colorToA = 255, n = self.label:sub(i,i)})
	end
	if self.options == false then else
		self.optionLetters = {}
		local offsetx = 510
		for i = 1, #self.options[self.value] do
    		self.optionLetters[i] = OptionObject.LETTER:new({x = self.x + indent + offsetx + letterSpacing * (i-1), y = self.y,  scale = 0.75, scaleTo = 0.25, fade = self.fadeDelay + OptionObject.SPEED*(#self.labelLetters + i), delay = self.fadeDelay + OptionObject.SPEED*(#self.labelLetters + i), fadeSpeed = OptionObject.SPEED, colorR = 255, colorG = 255, colorB = 255, colorA = 0, colorToA = 255, n = self.options[self.value]:sub(i,i)})	
		end
	end
end

function OptionObject:createOption()
	if self.options == false then else
		self.optionLetters = {}
		local letterSpacing = 40
		local indent = 30
		local offsetx = 510
		for i = 1, #self.options[self.value] do
    		self.optionLetters[i] = OptionObject.LETTER:new({x = self.x + indent + offsetx + letterSpacing * (i-1), y = self.y,  scale = 0.75, scaleTo = 0.25, fade = OptionObject.SPEED*i, delay = OptionObject.SPEED*i, fadeSpeed = OptionObject.SPEED, colorR = 255, colorG = 255, colorB = 255, colorA = 0, colorToA = 255, n = self.options[self.value]:sub(i,i)})	
		end
	end
end

function OptionObject:getDelay()
	return self.fadeDelay + OptionObject.SPEED*(#self.labelLetters + #self.optionLetters)
end

function OptionObject:getValue()
	return self.value
end

function OptionObject:incrementValue(v)
	if self.value == false or self.button then else
		self.value = self.value + v
		if self.value < 1 then self.value = #self.options end
		if self.value > #self.options then self.value = 1 end
		self:createOption() -- refresh option
	end
end

function OptionObject:select()
	if self.toggle then
		self:incrementValue(1)
	elseif self.button then
		if self.func == false then else
			self.func()
		end
	end
end

function OptionObject:setHighlight(v)
	self.highlight = v
	if self.highlight then
		self.color = { r = 255, g = 255, b = 255}
	else
		self.color = { r = 124, g = 124, b = 124}
	end
end

function OptionObject:setLevelComplete(v)
	for i,v in ipairs(self.labelLetters) do
		self.labelLetters[i]:startFade(0,1)
	end
	for i,v in ipairs(self.optionLetters) do
		self.optionLetters[i]:startFade(0,1)
	end
end

function OptionObject:update(dt) 	-- OptionObjects should color flash?
	for i,v in ipairs(self.labelLetters) do
		self.labelLetters[i]:setColor(self.color.r,self.color.g,self.color.b,self.color.a)
		self.labelLetters[i]:update(dt)
	end
	for i,v in ipairs(self.optionLetters) do
		self.optionLetters[i]:setColor(self.color.r,self.color.g,self.color.b,self.color.a)
		self.optionLetters[i]:update(dt)
	end
end

function OptionObject:draw()
	for i,v in ipairs(self.labelLetters) do
		self.labelLetters[i]:draw()
	end
	for i,v in ipairs(self.optionLetters) do
		self.optionLetters[i]:draw()
	end
end

return OptionObject
