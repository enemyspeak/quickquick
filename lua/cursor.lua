local Cursor = class('Cursor')

function Cursor:initialize(attributes)
	self.x = attributes.x or 0
	self.y = attributes.y or 0
	self.isRadial = attributes.isRadial or false

	if self.isRadial then
		self.radius = attributes.radius or 10
		self.centerX = attributes.centerX or 220
		self.centerY = attributes.centerY or 220
		self.xpos = self.x
		self.ypos = self.y
		self.deadzone = 0.4 * self.radius
	end

	self.size = 2
	self.trailLength = attributes.trailLength or 5
	self.trail = {}

	self.isClicked = false
end

function Cursor:setX(value)
	self.x = value
end

function Cursor:setY(value)
	self.y = value
end

function Cursor:getDirection()
	local tempX = self.x
	local tempY = self.y
	return math.atan2(tempY,tempX)
end

function Cursor:updateRadius()
	local P = 	{
				x = self.x,
				y = self.y
				}

	local tempX = P.x
	local tempY = P.y

	if math.abs(self.x) < self.deadzone and math.abs(self.y) < self.deadzone then
	else
		local h2 = tempX ^ 2 + tempY ^ 2 
		s =  math.sqrt(self.radius^2 / h2)

		self.xpos = self.centerX + tempX * s
		self.ypos = self.centerY + tempY * s
	end
end

function Cursor:update(dt)
	if self.isRadial then
		self:updateRadius()
		self.trail[#self.trail+1] = {x = self.xpos, y = self.ypos}
	else
		self.trail[#self.trail+1] = {x = self.x, y = self.y}
	end
	if #self.trail > self.trailLength then
		table.remove(self.trail,1)
	end
end

function Cursor:draw()
	local w = lg.getLineWidth()
    love.graphics.setLineWidth(self.size)
  --  lg.circle("line",self.centerX,self.centerY,self.radius)

    local x1, y1, x2, y2
	for i,v in ipairs(self.trail) do
		if i == 1 then 
			x1 = self.trail[1].x
			y1 = self.trail[1].y
		else
			lg.line( self.trail[i].x, self.trail[i].y,x1,y1)
			x1 = self.trail[i].x
			y1 = self.trail[i].y
		end
	end
	if self.isRadial then
		lg.circle("fill",self.xpos,self.ypos,self.size-0.5)
	else	
		lg.circle("fill",self.x,self.y,self.size-0.5)
	end

    love.graphics.setLineWidth(w)
end




return Cursor