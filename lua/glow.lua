local Glow = class('Glow')

Glow.static.ATLAS = lg.newImage('res/glow2.png')

Glow.static.ALPHA = 30

Glow.static.CENTERX = love.graphics.getWidth()/2
Glow.static.CENTERY = love.graphics.getHeight()/2

function Glow:initialize(attributes)
	local attributes = attributes or {}
	self.x = attributes.x or Glow.CENTERX
	self.y = attributes.y or Glow.CENTERY
	self.levelscale = attributes.scale or 1
	self.scale = 1
	self.alpha = Glow.ALPHA
	self.isEmpty = attributes.isEmpty
	
	self.color = 	{
					r = attributes.colorr or 255,
					g = attributes.colorg or 255,
					b = attributes.colorb or 255,
					}
end

function Glow:setLevelComplete()
	flux.to(self,0.5,{alpha = 0}):ease("quadin")
end

function Glow:setColor( color )
	self.color = color
end

function Glow:setEmpty(value)
	self.isEmpty = value
end

function Glow:update(dt)	
end

function Glow:getKill()
	return self.finished
end

function Glow:draw(value)
	love.graphics.setColor(self.color.r,self.color.g,self.color.b,self.alpha)
	if self.isEmpty then
		lg.draw(Glow.ATLAS,math.floor(self.x),math.floor(self.y),0,self.scale,self.scale,200/2,200/2)
	end
end

return Glow
