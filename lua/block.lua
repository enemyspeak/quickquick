local Block = class('Block')

Block.static.CENTERX = love.graphics.getWidth()/2
Block.static.CENTERY = love.graphics.getHeight()/2

Block.static.TILES = lg.newImage("res/tiles2.png")
Block.static.QUADS = 	{
						[0] = love.graphics.newQuad( 0, 0, 		30, 30, Block.TILES:getDimensions()),
						[1] = love.graphics.newQuad( 30, 0, 		30, 30, Block.TILES:getDimensions()),
						[2] = love.graphics.newQuad( 30*2, 0, 	30, 30, Block.TILES:getDimensions()),
						[3] = love.graphics.newQuad( 30*3, 0, 	30, 30, Block.TILES:getDimensions()),
						[4] = love.graphics.newQuad( 30*4, 0, 	30, 30, Block.TILES:getDimensions()),
						[5] = love.graphics.newQuad( 30*5, 0, 	30, 30, Block.TILES:getDimensions()),
						[6] = love.graphics.newQuad( 30*6, 0, 	30, 30, Block.TILES:getDimensions()),
						[7] = love.graphics.newQuad( 30*7, 0, 	30, 30, Block.TILES:getDimensions()),
						[8] = love.graphics.newQuad( 30*8, 0, 	30, 30, Block.TILES:getDimensions()),
						[9] = love.graphics.newQuad( 30*9, 0, 	30, 30, Block.TILES:getDimensions()),
						[10] = love.graphics.newQuad(30*10, 0, 	30, 30, Block.TILES:getDimensions()),
						[11] = love.graphics.newQuad(30*11, 0, 	30, 30, Block.TILES:getDimensions()),
						[12] = love.graphics.newQuad(30*12, 0, 	30, 30, Block.TILES:getDimensions()),
						[13] = love.graphics.newQuad(30*13, 0, 	30, 30, Block.TILES:getDimensions()),
						[14] = love.graphics.newQuad(30*14, 0, 	30, 30, Block.TILES:getDimensions()),
						[15] = love.graphics.newQuad(30*15, 0, 	30, 30, Block.TILES:getDimensions())
						}

function Block:initialize(attributes)
	self.offsetX = attributes.offsetX or 0
	self.offsetY = attributes.offsetY or 0

	self.x = attributes.x or 0
	self.y = attributes.y or 0
	self.gridX = attributes.gx or 0
	self.gridY = attributes.gy or 0

	self.width = 30
	self.height = 30

	self.n = attributes.n or 15
	self.scaleX = math.random(0,math.pi)
	self.scaleY = math.random(0,math.pi)
end

function Block:getN()
	return self.n
end

function Block:setHeight(value,distance)
	if value == 1 then
		self.playerHeight = distance
	elseif value == 2 then
		self.enemyHeight = distance
	end
end

function Block:getHeight(value)
	if value == 1 then
		return self.playerHeight
	elseif value == 2 then
		return self.enemyHeight
	end
end

function Block:getPosition()
	return self.x,self.y
end

function Block:setN(value)
	if value then
		self.n = value
	else
		self.n = 15
	end
end

function Block:update(dt)
	-- nothing to see here.
	self.scaleX = self.scaleX + dt
	self.scaleY = self.scaleY + dt
	if self.scaleX > math.pi then self.scaleX = 0 end
	if self.scaleY > math.pi then self.scaleY = 0 end
end

function Block:draw()
	--lg.setColor(27,27,27)
	lg.setColor(255,255,255)


	if self.n == 16 then else
		lg.setColor(255,255,255,255)
		local quad = Block.QUADS[self.n]
		lg.draw(Block.TILES,quad,self.offsetX + self.x*self.width,self.offsetY + self.y*self.height,0,1,1,0,0)
	end
end

return Block
