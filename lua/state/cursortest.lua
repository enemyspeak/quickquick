local Cursortest = Gamestate:addState('Cursortest')

local Cursor = require 'lua.cursor'

local WIDTH = lg.getWidth()
local HEIGHT = lg.getHeight()
local CENTERX = WIDTH/2
local CENTERY = HEIGHT/2
local debug_text = {}

local showDebug = false

local colors = {
				background = {14,6,31}
				}

local cursors = {}

local function debug(input, indent)
	if (input == nil) then
		input = "--------"
	end

	if (indent == nil) then
		indent = 0
	end

	local temp_string = tostring(input)

	for i = 1, indent, 1 do
		temp_string = "   " .. temp_string
	end

	table.insert(debug_text, temp_string)
end







--------

function Cursortest:enteredState()
	love.mouse.setVisible(false)
	cursors[1] = Cursor:new({x=0,y=0, isRadial = false})
end

function Cursortest:exitedstate()
	
end

function Cursortest:update(dt)
	local mx,my = love.mouse.getPosition()
	cursors[1]:setX(mx)
	cursors[1]:setY(my)

	for i,v in ipairs(cursors) do
		cursors[i]:update()
	end
end

function Cursortest:draw()
	love.graphics.setBackgroundColor(unpack(colors["background"]))
	lg.setColor(255,255,255,255)
	
	for i,v in ipairs(cursors) do
		cursors[i]:draw()
	end
	if showDebug then
	--	drawInfo()
	end
end

function Cursortest:keypressed(key, unicode)
	if key == 'n' then
		showDebug = not showDebug
	end
end

function Cursortest:keyreleased(key)

end

function Cursortest:joystickpressed(joystick,button)
	
end

function Cursortest:joystickadded(joystick)
end

function Cursortest:joystickremoved(joystick)
end

function Cursortest:mousepressed(x, y, button)
end

function Cursortest:mousereleased(x, y, button)
end
