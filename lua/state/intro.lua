local Intro = Gamestate:addState('Intro') 

local Player = require 'lua.player'
local Particle = require 'lua.particle'
local Wisp = require 'lua.wisp'
local Wind = require 'lua.wind'

local particles = {}

local wisp


local fog = lg.newImage("res/fog.png")

local WIDTH = lg.getWidth()
local HEIGHT = lg.getHeight()
local CENTERX = WIDTH/2
local CENTERY = HEIGHT/2

local debug_text = 	{}

local function debug(input, indent)
	if (input == nil) then
		input = "--------"
	end

	if (indent == nil) then
		indent = 0
	end

	local temp_string = tostring(input)

	for i = 1, indent, 1 do
		temp_string = "   " .. temp_string
	end

	table.insert(debug_text, temp_string);
end

local function updateParticles(dt)
	for i=1,#particles do
		if particles[i].windCooldownTime > Particle.WINDCOOLDOWN then
			local temp,angle,intensity = wind:testPoint(particles[i]:getPosition())
			if temp then
				particles[i]:setWind(angle,intensity)
			end
		end
		particles[i]:update(dt)
	end
end

local function drawParticles( ... )
	local m = lg.getCanvas()
	lg.setCanvas(renderBuffer)
	    love.graphics.clear( )

	for i=1,#particles do
		particles[i]:draw()
	end
	lg.setCanvas(m)
	lg.setColor(255,255,255)
	-- postshader:drawBloom(renderBuffer,{14.0,255})

	lg.draw(renderBuffer,0,0)
end

local function updateWind(dt)
	local mx,my = love.mouse.getPosition()
	wind:addPosition(mx,my)
	wind:update(dt)
end

local function drawWind( ... )
	wind:draw()
end

local function updateFog(dt)
	backgroundNoise = backgroundNoise + dt/10
end

local function drawFog( ... )
	--if backgroundNoise%0.5 < 0.1 then
		local size = 7
		for i=1,WIDTH/size do
			for j=1, HEIGHT/size do
				local a = love.math.noise(	i*0.005,
											j*0.01 + (math.cos(backgroundNoise*10)/j) + (math.cos(backgroundNoise*10)*j%5)/100--+math.cos(backgroundNoise+math.sin(backgroundNoise+j))/800),
											)--,math.sin(backgroundNoise) ) --
											--+ math.random()/10 * (math.cos(backgroundNoise/2 + j)/10) + math.random()/10

				if a > 1 then a = 1 end
				if a < 0.25 then
					lg.setColor(25,25,31)	
				elseif a < 0.45 then
					lg.setColor(28,24,31)	
				elseif a < 0.66 then
					lg.setColor(29,26,33)	
				elseif a < 0.9 then
					lg.setColor(29,29,34)
				else
					lg.setColor(29,28,34)	
				end
				lg.rectangle("fill",size*i-size,size*j-size,size,size)
			end
		end
	--end
end

--------

function Intro:enteredState()
	debug_text = {}
	renderBuffer = love.graphics.newCanvas(love.graphics.getWidth(), love.graphics.getHeight())
--	level = Level:new({})
	player = Player:new({x = 10, y = 10})
--	level:addPlayer(1,10,10)
	
	wind = Wind:new({})
	
	wisp = Wisp:new({
		x = WIDTH/2, 
		y = HEIGHT - 10,
		size = 2,
		speed = 4,
		pitch = 0.5
		})

	local particleLimit = 45
	particles ={}

	local killzone = Particle.KILLZONE
	for i = 1, particleLimit do
		particles[i] = Particle:new({
			x = math.random(0,WIDTH+killzone*2)-killzone, 
			y = math.random(0,HEIGHT+killzone*2)-killzone,
			size = math.random(1,5),
			speed = math.random(20,60),
			pitch = 0.1 + math.random() + math.random()
			})
	end
	backgroundNoise = 0
end

function Intro:update(dt)
--	level:update(dt)
--	player:update(dt)
	updateWind(dt)
	wisp:update(dt)
	updateParticles(dt)
	updateFog(dt)
end

function Intro:draw()
	-- renderBuffer:clear()
    love.graphics.clear( )


	    lg.setBackgroundColor(24,24,30)
  		lg.setColor(24,24,30)
	    lg.rectangle("fill",0,0,WIDTH,HEIGHT)
		
		---[[
		
		--lg.draw(fog,100,100)
		--]]

--		drawFog()
	--	drawWind()
		drawParticles()
--		wisp:draw()

--		level:draw()
--		player:draw()

	for index,value in pairs(debug_text) do
		love.graphics.print(value, 100, 100 + 12*index)
	end
	debug_text = {}	
end

function Intro:keypressed(key, unicode)
	if key == "n" then
		level = Level:new()
		player = Player:new({x = 10, y = 10})
	end
end

function Intro:joystickadded(j)
end

function Intro:joystickpressed(n,b)
end

function Intro:keyreleased(key)
end

function Intro:mousepressed(x, y, button)
end

function Intro:mousereleased(x, y, button)
end
