local Menu = Gamestate:addState('Menu')

local Letter = require 'lua.letter'
local Player = require 'lua.player'

local WIDTH =  IRESX --lg.getWidth()
local HEIGHT = IRESY --lg.getHeight()
local CENTERX = WIDTH/2
local CENTERY = HEIGHT/2

local canvasBuffer = love.graphics.newCanvas(IRESX, IRESY)
local canvasBuffer2 = love.graphics.newCanvas(IRESX/2, IRESY/2)

local BETTERGLOW = lg.newImage('res/glow2.png')

local shadersEnabled = true
local glowsEnabled = true

local settings = 	{}
local controls =	{
						{
							"w",
							"a",
							"s",
							"d",
							"space"
						},
						{
							"kp8",
							"kp4",
							"kp5",
							"kp6",
							"kp0"
						}
					}

local title = 	{}
local players = {}

local debug_text = 	{}

local function debug(input, indent)
	if (input == nil) then
		input = "--------"
	end

	if (indent == nil) then
		indent = 0
	end

	local temp_string = tostring(input)

	for i = 1, indent, 1 do
		temp_string = "   " .. temp_string
	end

	table.insert(debug_text, temp_string);
end

function updateTitle(dt) 
	for i,v in ipairs(title) do
		title[i]:update(dt)
	end
end

function drawTitle() 
	for i,v in ipairs(title) do
		title[i]:draw()
	end
end

local function drawBetterGlow()
	local color = {r = 255,g = 255,b = 225}
	local mode = love.graphics.getBlendMode( )
	lg.setColor(color.r,color.g,color.b,100)
	lg.draw(BETTERGLOW,IRESX/2,IRESY/2,0,3,3,720/2,720/2)
	love.graphics.setBlendMode( mode )
end

local function updatePlayers(dt)
	for i,v in ipairs(players) do
		players[i]:keypressed()
		players[i]:update(dt)
	end
end

local function drawPlayers()
	for i,v in ipairs(players) do
		players[i]:draw()
	end
end

--------

function Menu:enteredState()
	debug_text = {}
	renderBuffer = love.graphics.newCanvas(love.window.getWidth(), love.window.getHeight())

	local xpos = 0
	local ypos = -200
	local d = 15
	
	title = {}
	title[1] = 	 Letter:new({ x = xpos-305,y = ypos,n = 'q', popup = true,elastic = true,fade = #title/d-0.05, fadeSpeed = 0.2, scale = 0.5, colorR = 255, colorG = 255, colorB = 255, colorA = 0, popupTime = 0.25,delay = #title/d })
	title[2] = 	 Letter:new({ x = xpos-240,y = ypos,n = 'u', popup = true,elastic = true,fade = #title/d-0.05, fadeSpeed = 0.2, scale = 0.5, colorR = 255, colorG = 255, colorB = 255, colorA = 0, popupTime = 0.25,delay = #title/d })
	title[3] = 	 Letter:new({ x = xpos-155,y = ypos,n = 'i', popup = true,elastic = true,fade = #title/d-0.05, fadeSpeed = 0.2, scale = 0.5, colorR = 255, colorG = 255, colorB = 255, colorA = 0, popupTime = 0.25,delay = #title/d })
	title[4] = 	 Letter:new({ x = xpos-90, y = ypos,n = 'c', popup = true,elastic = true,fade = #title/d-0.05, fadeSpeed = 0.2, scale = 0.5, colorR = 255, colorG = 255, colorB = 255, colorA = 0, popupTime = 0.25,delay = #title/d })
	title[5] = 	 Letter:new({ x = xpos-30, y = ypos,n = 'k', popup = true,elastic = true,fade = #title/d-0.05, fadeSpeed = 0.2, scale = 0.5, colorR = 255, colorG = 255, colorB = 255, colorA = 0, popupTime = 0.25,delay = #title/d })
	title[6] = 	 Letter:new({ x = xpos+105,y = ypos,n = 'q', popup = true,elastic = true,fade = #title/d-0.05, fadeSpeed = 0.2, scale = 0.5, colorR = 255, colorG = 255, colorB = 255, colorA = 0, popupTime = 0.25,delay = #title/d })
	title[7] = 	 Letter:new({ x = xpos+165,y = ypos,n = 'u', popup = true,elastic = true,fade = #title/d-0.05, fadeSpeed = 0.2, scale = 0.5, colorR = 255, colorG = 255, colorB = 255, colorA = 0, popupTime = 0.25,delay = #title/d })
	title[8] = 	 Letter:new({ x = xpos+255,y = ypos,n = 'i', popup = true,elastic = true,fade = #title/d-0.05, fadeSpeed = 0.2, scale = 0.5, colorR = 255, colorG = 255, colorB = 255, colorA = 0, popupTime = 0.25,delay = #title/d })
	title[9] = 	 Letter:new({ x = xpos+305,y = ypos,n = 'c', popup = true,elastic = true,fade = #title/d-0.05, fadeSpeed = 0.2, scale = 0.5, colorR = 255, colorG = 255, colorB = 255, colorA = 0, popupTime = 0.25,delay = #title/d })
	title[10] =  Letter:new({ x = xpos+355,y = ypos,n = 'k', popup = true,elastic = true,fade = #title/d-0.05, fadeSpeed = 0.2, scale = 0.5, colorR = 255, colorG = 255, colorB = 255, colorA = 0, popupTime = 0.25,delay = #title/d })

	players = {}
	players[1] = Player:new()
end

function Menu:update(dt)
	updateTitle(dt)
	updatePlayers(dt)
end

function Menu:draw()
	local canvas = lg.getCanvas()
	canvasBuffer:clear()
	canvasBuffer2:clear()

	lg.setCanvas(canvasBuffer)

	lg.setColor(255,255,255)
	love.graphics.push()
	lg.translate(CENTERX,CENTERY)

		drawTitle()
		drawPlayers()

	love.graphics.pop()
	if shadersEnabled then
		love.graphics.setCanvas(canvasBuffer2)	
			love.graphics.push()
			love.graphics.origin()
				lg.setColor(15,16,46)
				lg.rectangle("fill",0,0,IRESX/2,IRESY/2)
			    love.graphics.setColor(255,255,255)
			    love.graphics.draw(canvasBuffer,0,0,0,0.5,0.5)
			love.graphics.pop()
		love.graphics.setCanvas(canvas)
			lg.setColor(15,16,46)
			lg.rectangle("fill",0,0,WIDTH,HEIGHT)
			
			if glowsEnabled then
				drawBetterGlow()
				postshader:drawBloom(canvasBuffer2,{14.0,255})
			end

			lg.setColor(255,255,255,100)
			lg.draw(canvasBuffer2,0,0,0,2,2)
		
			lg.setColor(255,255,255,255)
			lg.draw(canvasBuffer,0,0)
			postshader:drawTiltShift(canvas,{6.0,6.0})
	else
		love.graphics.setCanvas(canvas)	
			lg.setColor(15,16,46)
			lg.rectangle("fill",0,0,WIDTH,HEIGHT)

			if glowsEnabled then
				drawBetterGlow()
			end

			lg.setColor(255,255,255,255)
			lg.draw(canvasBuffer,0,0)
	end

	lg.setColor(255,255,255,255)
	for index,value in pairs(debug_text) do
		love.graphics.print(value, 100, 100 + 12*index)
	end

	debug_text = {}
end

function Menu:keypressed(key, unicode)
end

function Menu:joystickadded(j)
end

function Menu:joystickpressed(n,b)
end

function Menu:keyreleased(key)
end

function Menu:mousepressed(x, y, button)
end

function Menu:mousereleased(x, y, button)
end
