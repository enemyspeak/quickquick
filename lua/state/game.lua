-- game.lua
-- this is a videogame man

local Game = Gamestate:addState('Game')

local Button = require 'lua.button'
local Letter = require 'lua.letter'
local Level = require 'lua.level'

local WIDTH =  IRESX --lg.getWidth()
local HEIGHT = IRESY --lg.getHeight()
local CENTERX = WIDTH/2
local CENTERY = HEIGHT/2

local gameLetters = {}

local settings = {}
local controls =	{
						{
							"q",
							"e",
							"c",
							"z",
							"s"
						},
					}

local letterColors = {
					r = 255,
					g = 255,
					b = 255,
					a = 0,
					n = 0
					}

local levelNum = 1
local difficultyCurve = 0.25

local debug_text = {}

local function debug(input, indent)
	if (input == nil) then
		input = "--------"
	end

	if (indent == nil) then
		indent = 0
	end

	local temp_string = tostring(input)

	for i = 1, indent, 1 do
		temp_string = "   " .. temp_string
	end

	table.insert(debug_text, temp_string);
end


local function createLevel()
	local w = math.random(10,16)
	local h = math.random(10,16)
	while w*h < 160 do 
		w = math.random(6,26)
		h = math.random(6,22)
	end

	local gridsize = 32
	local gridX = 0
	local gridY = 0
	local x = 0--(((lg.getWidth()-levelwidth)/2)/2)/0.4 --(h/2-w/2) * (111 / 2) -- 111/2
	local y = 0-- /0.4-- (h/2 + w/2) * (65/2) - (65 * (w/2)) -- 65/2

	local colorSet = math.random(1,4)
	colorSet = 3

	local color,topColor,glowColor,glowTopColor -- these should be level variables

	if colorSet == 1 then
		color = { r = 42, g = 0, b = 131, a = 0}
		topColor = { r = 119, g = 0, b = 188}
		glowColor = { r = 177, g = 44, b = 177}
		glowTopColor = { r = 229, g = 136, b = 213}
	elseif colorSet == 2 then
		color = { r = 34, g = 169, b = 119, a = 0}
		topColor = { r = 0, g = 255, b = 255}
		glowColor = { r = 178, g = 174, b = 79}
		glowTopColor = { r = 194, g = 255, b = 66}
	elseif colorSet == 3 then
		color = { r = 94, g = 16, b = 36, a = 0}
		topColor = { r = 122, g = 0, b = 84}
		glowColor = { r = 36, g = 17, b = 187}
		glowTopColor = { r = 118, g = 177, b = 221}
	elseif colorSet == 4 then
		color = { r = 163, g = 83, b = 63, a = 0}
		topColor = { r = 211, g = 115, b = 84}
		glowColor = { r = 36, g = 17, b = 187}
		glowTopColor = { r = 88, g = 161, b = 168}
	end

	levelNum = levelNum + 1
	difficultyCurve = 0.25
	local spawnRate = ((levelNum * difficultyCurve) * ((w*h)/160)) * 50

	level = Level:new({levelNum = levelNum,spawnRate = spawnRate,numPlayers = 1, width=w,height=h, x = x, y = y, color = color, topColor = topColor, glowColor = glowColor, glowTopColor = glowTopColor })
end

local function updateTitle( dt )
	for i,v in ipairs(gameLetters) do
		gameLetters[i]:update(dt)
		gameLetters[i]:setColor(letterColors["r"],letterColors.g,letterColors.b,letterColors.a)
	end
end

local function drawTitle()
	lg.push()
	lg.translate(-65,0)

	for i,v in ipairs(gameLetters) do
		gameLetters[i]:draw()
	end

	lg.pop()
end

local function updateLevel(dt)
	level:update(dt)
end

local function drawLevel( ... )
	lg.setColor(255,255,255)
	level:draw()
end

local function tweenLetterColor()
	flux.to(letterColors, 1, { r = 193, g = 255, b = 85, a=255 }):ease("linear"):after(
		letterColors, 1, { r = 232, g = 133, b = 61 }):ease("linear"):after(
			letterColors, 1, { r = 168, g = 20, b = 255  }):ease("linear"):after(
				letterColors, 1, { r = 0, g = 232, b = 222 }):ease("linear"):after(
					letterColors, 1, { r = 255, g = 255, b = 7, n = 0 }):ease("linear")
end

local function HSL(h, s, l, a)
    if s<=0 then return l,l,l,a end
    h, s, l = h/256*6, s/255, l/255
    local c = (1-math.abs(2*l-1))*s
    local x = (1-math.abs(h%2-1))*c
    local m,r,g,b = (l-.5*c), 0,0,0
    if h < 1     then r,g,b = c,x,0
    elseif h < 2 then r,g,b = x,c,0
    elseif h < 3 then r,g,b = 0,c,x
    elseif h < 4 then r,g,b = 0,x,c
    elseif h < 5 then r,g,b = x,0,c
    else              r,g,b = c,0,x
    end return (r+m)*255,(g+m)*255,(b+m)*255,a
end

local function debugFinishLevel()
	level:setFinished(true)
end









--------

function Game:enteredState()
	letterColors = {
					r = 255,
					g = 255,
					b = 255,
					a = 0,
					n = 0
					}

	gameLetters	= {}
	debug_text = {}

	renderBuffer = love.graphics.newCanvas(love.window.getWidth(), love.window.getHeight())

	local w = 8
	local h = 8
	
	local gridsize = 32
	local gridX = 0
	local gridY = 0
	
--[[
	self.posX = self.offsetX + ((self.y-self.x) * (Player.BLOCKWIDTH / 2))
	self.posY = self.offsetY + ((self.x+self.y) * (Player.BLOCKDEPTH / 2)) - (Player.BLOCKDEPTH * (self.width / 2))
--]]
	--local levelwidth = ((0-w) * (111 / 2)) - ((h-0) * (111 / 2))
	--local levelheight


	local x = 0--(((lg.getWidth()-levelwidth)/2)/2)/0.4 --(h/2-w/2) * (111 / 2) -- 111/2
	local y = 0-- /0.4-- (h/2 + w/2) * (65/2) - (65 * (w/2)) -- 65/2

	local colorSet = math.random(1,4)
	colorSet = 3

	local color,topColor,glowColor,glowTopColor

	if colorSet == 1 then
		color = { r = 42, g = 0, b = 131, a = 0}
		topColor = { r = 119, g = 0, b = 188}
		glowColor = { r = 177, g = 44, b = 177}
		glowTopColor = { r = 229, g = 136, b = 213}
	elseif colorSet == 2 then
		color = { r = 34, g = 169, b = 119, a = 0}
		topColor = { r = 0, g = 255, b = 255}
		glowColor = { r = 178, g = 174, b = 79}
		glowTopColor = { r = 194, g = 255, b = 66}
	elseif colorSet == 3 then
		color = { r = 94, g = 16, b = 36, a = 0}
		topColor = { r = 122, g = 0, b = 84}
		glowColor = { r = 36, g = 17, b = 187}
		glowTopColor = { r = 118, g = 177, b = 221}
	elseif colorSet == 4 then
		color = { r = 163, g = 83, b = 63, a = 0}
		topColor = { r = 211, g = 115, b = 84}
		glowColor = { r = 36, g = 17, b = 187}
		glowTopColor = { r = 88, g = 161, b = 168}
	end

	levelNum = 0
	difficultyCurve = 0.25
	local spawnRate = 0
	local firstLevel = true

	level = Level:new({levelNum = levelNum, firstLevel = firstLevel, spawnRate = spawnRate,numPlayers = settings.numPlayers or 1, width=w,height=h, x = x, y = y, color = color, topColor = topColor, glowColor = glowColor, glowTopColor = glowTopColor })
end

function Game:update(dt)
	for i,v in ipairs(controls) do
		for j,k in ipairs(controls[i]) do
			if love.keyboard.isDown(controls[i][j]) then
				level:keypressed(i,j)
			end
			if j == 5 then
				level:keyreleased(i,j)
			end
		end
	end

	updateLevel(dt)
	
	if letterColors.n == 0 then
		letterColors.n = 1
		tweenLetterColor()
	end
end

function Game:draw()
	renderBuffer:clear()
	local canvas = love.graphics.getCanvas()
    love.graphics.setCanvas(renderBuffer)
	love.graphics.push()
	lg.translate(CENTERX,CENTERY)	
	lg.setColor(15,16,46)
	lg.rectangle("fill",-CENTERX,-CENTERY,WIDTH,HEIGHT)
 	lg.setColor(255,255,255)

	lg.scale(level:getScale())
	love.graphics.translate(level:getShakeX(),level:getShakeY())
		
		lg.setColor(255,255,255)
		drawLevel()

	love.graphics.pop()
	love.graphics.setCanvas(canvas)
	lg.draw(renderBuffer,0,0)

	lg.setColor(255,255,255,255)
	for index,value in pairs(debug_text) do
		love.graphics.print(value, 100, 100 + 12*index)
	end

	debug_text = {}	
end

function Game:keypressed(key, unicode)
	if key =='n' then
		--Game:enteredState()
	--	debugFinishLevel()
	

end

function Game:joystickadded(j)
	
end

function Game:joystickpressed(n,b)
	local v

	if n:isGamepadDown("a") then
		level:keypressed(1,v)
	end
end

function Game:keyreleased(key)
end

function Game:mousepressed(x, y, button)
end

function Game:mousereleased(x, y, button)
end
