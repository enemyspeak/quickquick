local Arcade = Gamestate:addState('Arcade')

local Letter = require 'lua.letter'
local ArcadeObject = require 'lua.gameobject'

local WIDTH = lg.getWidth()
local HEIGHT = lg.getHeight()
local CENTERX = WIDTH/2
local CENTERY = HEIGHT/2

local controls =	{
						{
							"q",
							"e",
							"c",
							"z",
							"s"
						},
						{
							"kp7",
							"kp9",
							"kp3",
							"kp1",
							"kp5"
						}
					}
local debug_text = 	{}

local function debug(input, indent)
	if (input == nil) then
		input = "--------"
	end

	if (indent == nil) then
		indent = 0
	end

	local temp_string = tostring(input)

	for i = 1, indent, 1 do
		temp_string = "   " .. temp_string
	end

	table.insert(debug_text, temp_string);
end









--------

function Arcade:enteredState()
	debug_text = {}
	renderBuffer = love.graphics.newCanvas(love.window.getWidth(), love.window.getHeight())

	local w = 8
	local h = 8
	local x = 0
	local y = 0
	local color = { r = 42, g = 0, b = 131, a = 0}
	local topColor = { r = 119, g = 0, b = 188}
	local glowColor = { r = 177, g = 44, b = 177}
	local glowTopColor = { r = 229, g = 136, b = 213}
	level = ArcadeObject:new({width = w, height = h, x = x, y = y, color = color, topColor = topColor, glowColor = glowColor, glowTopColor = glowTopColor })
end

function Arcade:update(dt)
	level:update(dt)
	if level.completed then
		gamestate:gotoState("Menu")
	end
end

function Arcade:draw()
	lg.setBackgroundColor(15,16,46)
	lg.push()
	lg.translate(CENTERX,CENTERY)	
		lg.setColor(255,255,255)
		level:draw()
	love.graphics.pop()

	for index,value in pairs(debug_text) do
		love.graphics.print(value, 100, 100 + 12*index)
	end
	debug_text = {}	
end

function Arcade:keypressed(key, unicode)
	if key == 'l' then
		level:toggleShaders()
	elseif key == 'n' then
		local w = 8
		local h = 8
		local x = 0
		local y = 0
		local color = { r = 42, g = 0, b = 131, a = 0}
		local topColor = { r = 119, g = 0, b = 188}
		local glowColor = { r = 177, g = 44, b = 177}
		local glowTopColor = { r = 229, g = 136, b = 213}
		level = ArcadeObject:new({width = w, height = h, x = x, y = y, color = color, topColor = topColor, glowColor = glowColor, glowTopColor = glowTopColor })
	end
end

function Arcade:joystickadded(j)

end

function Arcade:joystickpressed(n,b)

end

function Arcade:keyreleased(key)

end

function Arcade:mousepressed(x, y, button)

end

function Arcade:mousereleased(x, y, button)
	
end
