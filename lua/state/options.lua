local Options = Gamestate:addState('Options')

local Letter = require 'lua.letter'
local Streak = require 'lua.streak'
local OptionObject = require 'lua.optionobject'

local WIDTH =  IRESX --lg.getWidth()
local HEIGHT = IRESY --lg.getHeight()
local CENTERX = WIDTH/2
local CENTERY = HEIGHT/2


local streaks = {}
local streakBuffer = 0
local optionSelected = 1

local fade = {alpha = 0}
local optionMenu = {}
local titleLetters = {}

local controls = {}

local debug_text = 	{}

local function debug(input, indent)
	if (input == nil) then
		input = "--------"
	end

	if (indent == nil) then
		indent = 0
	end

	local temp_string = tostring(input)

	for i = 1, indent, 1 do
		temp_string = "   " .. temp_string
	end

	table.insert(debug_text, temp_string);
end

local function saveOptions() -- save stuff, apply fullscreen, apply resolution, THIS SHOULD WRITE TO FILE
	if optionMenu[2]:getValue() == 1 then -- resolution
		local w,h = love.graphics.getDimensions( )
		if w == 720 and h == 480 then else
			love.window.setMode( 720, 480 )
		end
	elseif optionMenu[2]:getValue() == 2 then
		local w,h = love.graphics.getDimensions( )
		if w == 1280 and h == 720 then else
			love.window.setMode( 1280, 720 )
		end
	elseif optionMenu[2]:getValue() == 3 then
		if w == 1920 and h == 1080 then else
			love.window.setMode( 1920, 1080 )
		end
	elseif optionMenu[2]:getValue() == 4 then
		if w == 2560 and h == 1440 then else
			love.window.setMode( 2560, 1440 )
		end
	end
	if optionMenu[3]:getValue() == 1 then -- fullscreen on
		local f = love.window.getFullscreen()
		if f then else
			love.window.setFullscreen( true )
		end
	elseif optionMenu[3]:getValue() == 2 then -- off
		local f = love.window.getFullscreen()
		if f then 
			love.window.setFullscreen( false )
		end
	end
end

local function createMenu()	-- this is where we make the different menu options
	optionMenu = {}
	local settings = {
					[1] = 1,
					[2] = 2,
					[3] = 2,
					[4] = 1,
					[5] = 1,
					[6] = 1,
					[7] = 1,
					}
	local offsety = -200
	local yspacing = 50
	local offsetx = -500
	optionMenu[1] = OptionObject:new({y = offsety+yspacing*1, x = offsetx, value = settings[1], label = "multiplayer", options = {[1] = "cooperative",[2] = "competitive",[3] = "competitive+"}, highlight = true })
	optionMenu[2] = OptionObject:new({y = offsety+yspacing*2, x = offsetx, fadeDelay = optionMenu[1]:getDelay() + 0.05, value = settings[2], label = "resolution", options = {[1] = "568x320",[2] = "720x480",[4] = "1280x720",[4] = "1920x1080"}})
	optionMenu[3] = OptionObject:new({y = offsety+yspacing*3, x = offsetx, fadeDelay = optionMenu[2]:getDelay() + 0.05, value = settings[3], label = "fullscreen", options = {[1] = "on",[2] = "off"}, toggle = true})
	optionMenu[4] = OptionObject:new({y = offsety+yspacing*4, x = offsetx, fadeDelay = optionMenu[3]:getDelay() + 0.05, value = settings[4], label = "hard mode", options = {[1] = "on",[2] = "off"}, toggle = true})
	optionMenu[5] = OptionObject:new({y = offsety+yspacing*5, x = offsetx, fadeDelay = optionMenu[4]:getDelay() + 0.05, value = settings[5], label = "shaders", options = {[1] = "on",[2] = "off"}, toggle = true})
	optionMenu[6] = OptionObject:new({y = offsety+yspacing*6, x = offsetx, fadeDelay = optionMenu[5]:getDelay() + 0.05, value = settings[6], label = "glow", options = {[1] = "on",[2] = "off"}, toggle = true})
	optionMenu[7] = OptionObject:new({y = offsety+yspacing*7, x = offsetx, fadeDelay = optionMenu[6]:getDelay() + 0.05, value = settings[7], label = "sound", options = {[1] = "on",[2] = "off"}, toggle = true})
	local f = function() 
		local t = function() gamestate:gotoState("Controls") end
		saveOptions()
		flux.to(fade,1,{alpha = 255}):oncomplete(t):ease("quadout")
	end 
	optionMenu[8] = OptionObject:new({y = offsety+yspacing*8, x = offsetx, fadeDelay = optionMenu[7]:getDelay() + 0.05, label = "set controls", button = true, func = f})
	local f = function() 
		local t = function() gamestate:gotoState("Menu") end
		saveOptions()
		flux.to(fade,1,{alpha = 255}):oncomplete(t):ease("quadout")
	 end 
	optionMenu[9] = OptionObject:new({y = offsety+yspacing*9, x = offsetx, fadeDelay = optionMenu[8]:getDelay() + 0.05, label = "back", button = true, func = f })
end

local function updateStreaks(dt)
	local interval = 0.1
	streakBuffer = streakBuffer + dt
	if streakBuffer > interval then
		streakBuffer = 0
		local x = Streak.WIDTH
		local offset = 100
		local y = math.random(0,CENTERY-offset)
		local s = math.random(0,1)
		if s == 1 then
			y = -y
		end
		streaks[#streaks + 1] = Streak({x = x, y = y})
	end
	for i,v in ipairs(streaks) do
		if streaks[i]:getKill() then
			table.remove(streaks,i)
			break
		end
	end
end

local function updateTitle(dt)
	for i,v in ipairs(titleLetters) do
		titleLetters[i]:update(dt)
	end
end

local function updateMenu(dt)
	for i,v in ipairs(optionMenu) do
		if i == optionSelected then
			optionMenu[i]:setHighlight(true)
		else
			optionMenu[i]:setHighlight(false)
		end
		optionMenu[i]:update(dt)
	end
end

local function drawStreaks()
	for i,v in ipairs(streaks) do
		streaks[i]:draw()
	end
end

local function drawTitle()
	for i,v in ipairs(titleLetters) do
		titleLetters[i]:draw()
	end
end

local function drawMenu()
	for i,v in ipairs(optionMenu) do
		optionMenu[i]:draw()
	end
end









--------

function Options:enteredState()
	debug_text = {}
	streaks = {}
	fade = {alpha = 0}
	streakBuffer = 0
	optionSelected = 1
	local offset = -465
	local offsety = -265
	local amp = 7
	titleLetters = 	{
					[2] = Letter:new({ripple = true, amp = amp, lifespan = false, scaleSpeed = 0.25, scale = 0.05, scaleTo = 0.6, fadeSpeed = 0.5, colorR = 255, colorG = 255, colorB = 255, colorA = 0, colorToA = 255, fade = 0.1, delay = 0.1, 	x = offset+50, 	y = offsety + 0,n = "p"}),
					[1] = Letter:new({ripple = true, amp = amp, lifespan = false, scaleSpeed = 0.25, scale = 0.05, scaleTo = 0.6, fadeSpeed = 0.5, colorR = 255, colorG = 255, colorB = 255, colorA = 0, colorToA = 255, fade = 0.05, delay = 0.05, x = offset, 		y = offsety + 0,n = "o"}),
					[3] = Letter:new({ripple = true, amp = amp, lifespan = false, scaleSpeed = 0.25, scale = 0.05, scaleTo = 0.6, fadeSpeed = 0.5, colorR = 255, colorG = 255, colorB = 255, colorA = 0, colorToA = 255, fade = 0.15, delay = 0.15, x = offset+95, 	y = offsety + 0,n = "t"}),
					[4] = Letter:new({ripple = true, amp = amp, lifespan = false, scaleSpeed = 0.25, scale = 0.05, scaleTo = 0.6, fadeSpeed = 0.5, colorR = 255, colorG = 255, colorB = 255, colorA = 0, colorToA = 255, fade = 0.2, delay = 0.2, 	x = offset+140, y = offsety + 0,n = "i"}),
					[5] = Letter:new({ripple = true, amp = amp, lifespan = false, scaleSpeed = 0.25, scale = 0.05, scaleTo = 0.6, fadeSpeed = 0.5, colorR = 255, colorG = 255, colorB = 255, colorA = 0, colorToA = 255, fade = 0.25, delay = 0.25, x = offset+185, 	y = offsety + 0,n = "o"}),
					[6] = Letter:new({ripple = true, amp = amp, lifespan = false, scaleSpeed = 0.25, scale = 0.05, scaleTo = 0.6, fadeSpeed = 0.5, colorR = 255, colorG = 255, colorB = 255, colorA = 0, colorToA = 255, fade = 0.3, delay = 0.3, 	x = offset+235, y = offsety + 0,n = "n"}),
					[7] = Letter:new({ripple = true, amp = amp, lifespan = false, scaleSpeed = 0.25, scale = 0.05, scaleTo = 0.6, fadeSpeed = 0.5, colorR = 255, colorG = 255, colorB = 255, colorA = 0, colorToA = 255, fade = 0.35, delay = 0.35, x = offset+280, 	y = offsety + 2.5,n = "s"})
					}
	createMenu()
end

function Options:update(dt)
	for i,v in ipairs(controls) do
		for j,k in ipairs(controls[i]) do
			if love.keyboard.isDown(controls[i][j]) then
		--		level:keypressed(i,j)
			end
			if j == 5 then
--				level:keyreleased(i,j)
			end
		end
	end
	updateStreaks(dt)
	updateTitle(dt)
	updateMenu(dt)
end

function Options:draw()
	lg.setBackgroundColor(15,16,46)
	lg.push()
	lg.translate(CENTERX,CENTERY)	
		drawStreaks()
		drawTitle()
		drawMenu()
		lg.setColor(15,16,46,fade.alpha)
		lg.rectangle("fill",-CENTERX,-CENTERY,WIDTH,HEIGHT)
	love.graphics.pop()

	for index,value in pairs(debug_text) do
		love.graphics.print(value, 100, 100 + 12*index)
	end
	debug_text = {}	
end

function Options:keypressed(key, unicode)
	if key == "down" then
		optionSelected = optionSelected + 1
		if optionSelected > #optionMenu then optionSelected = #optionMenu end
	elseif key == "up" then
		optionSelected = optionSelected - 1
		if optionSelected < 1 then optionSelected = 1 end
	elseif key == "right" then
		optionMenu[optionSelected]:incrementValue(1)
	elseif key == "left" then 
		optionMenu[optionSelected]:incrementValue(-1)
	elseif key == "return" then
		optionMenu[optionSelected]:select()
	end
end

function Options:joystickadded(j)
end

function Options:joystickpressed(n,b)
end

function Options:keyreleased(key)
end

function Options:mousepressed(x, y, button)
end

function Options:mousereleased(x, y, button)
end
