local Controllertest = Gamestate:addState('Controllertest')

local Letter = require 'lua.letter'
local Dualshock = require 'lua.dualshock'

local WIDTH = lg.getWidth()
local HEIGHT = lg.getHeight()
local CENTERX = WIDTH/2
local CENTERY = HEIGHT/2
local debug_text = {}

local showDebug = true

local colors = {
				background = {14,6,31}
				}

local function debug(input, indent)
	if (input == nil) then
		input = "--------"
	end

	if (indent == nil) then
		indent = 0
	end

	local temp_string = tostring(input)

	for i = 1, indent, 1 do
		temp_string = "   " .. temp_string
	end

	table.insert(debug_text, temp_string)
end

function drawJoystickInfo(...)
	lg.setColor(255,255,255,255)
	local font = love.graphics.getFont()
	local paddingh = 5
	local w = font:getWidth("Joysticks  Connected: ")
	lg.print("Joysticks Connected: "..#joysticks,-(w/2),-CENTERY+paddingh)
end







--------

function Controllertest:enteredState()

end

function Controllertest:exitedstate()
	
end

function Controllertest:update(dt)
	for i,v in ipairs(dualshocks) do
		dualshocks[i]:update(dt)
	end
end

function Controllertest:draw()
	love.graphics.setBackgroundColor(unpack(colors["background"]))
	lg.setColor(255,255,255,255)
	
	for i,v in ipairs(dualshocks) do
		dualshocks[i]:draw()
	end
	
	if showDebug then
		drawJoystickInfo()
		local row = 1
		local reset = -(12*26)
		for index,value in pairs(debug_text) do
			love.graphics.print(value, 100*row, 100 + 12*index + reset*(row-1))
			if index/row > 26 then
				row = row + 1
			end
		end
		--debug_text = {}	
	end

end

function Controllertest:keypressed(key, unicode)
	if key == 'n' then
		showDebug = not showDebug
	end
end

function Controllertest:keyreleased(key)

end

function Controllertest:joystickpressed(joystick,button)
	debug(button)
end

function Controllertest:joystickadded(joystick)
	dualshocks[#dualshocks+1] = Dualshock:new({x = CENTERX, y = CENTERY,input = joystick})	
end

function Controllertest:joystickremoved(joystick)
	dualshocks[joystick] = nil
end

function Controllertest:mousepressed(x, y, button)
end

function Controllertest:mousereleased(x, y, button)
end
