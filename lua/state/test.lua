local Test = Gamestate:addState('Test') -- develop environment for various things

local Letter = require 'lua.letter'
local Level = require 'lua.level'
local Player = require 'lua.player'

local WIDTH = lg.getWidth()
local HEIGHT = lg.getHeight()
local CENTERX = WIDTH/2
local CENTERY = HEIGHT/2

local settings = 	{}
local controls =	{
						{
							"q",
							"e",
							"c",
							"z",
							"s"
						},
						{
							"kp7",
							"kp9",
							"kp3",
							"kp1",
							"kp5"
						}
					}
local debug_text = 	{}

local function debug(input, indent)
	if (input == nil) then
		input = "--------"
	end

	if (indent == nil) then
		indent = 0
	end

	local temp_string = tostring(input)

	for i = 1, indent, 1 do
		temp_string = "   " .. temp_string
	end

	table.insert(debug_text, temp_string);
end









--------

function Test:enteredState()
	debug_text = {}
	renderBuffer = love.graphics.newCanvas(love.window.getWidth(), love.window.getHeight())
	level = Level:new()
	player = Player:new({x = 10, y = 10})
	level:addPlayer(1,10,10)
	
end

function Test:update(dt)
	level:update(dt)
	player:update(dt)
end

function Test:draw()
	renderBuffer:clear()
	lg.scale(0.75)

		level:draw()
		player:draw()

	for index,value in pairs(debug_text) do
		love.graphics.print(value, 100, 100 + 12*index)
	end
	debug_text = {}	
end

function Test:keypressed(key, unicode)
	if key == "n" then
		level = Level:new()
		player = Player:new({x = 10, y = 10})
	end
end

function Test:joystickadded(j)
end

function Test:joystickpressed(n,b)
end

function Test:keyreleased(key)
end

function Test:mousepressed(x, y, button)
end

function Test:mousereleased(x, y, button)
end
