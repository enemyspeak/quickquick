---------------------------------------------------------
--Button code originally based on code by Michael Enger--
---------------------------------------------------------

local Button = class('Button')

Button.static.BACKGROUND = {57,57,57}
Button.static.NORMAL = {174,174,174}
Button.static.HOVER = {255,255,255}

function Button:initialize(attributes)
	self.height = attributes.height or 55
	self.width = attributes.width or 50

	self.x = attributes.x
	self.y = attributes.y

	self.label = attributes.label or false

	self.hover = false -- whether the mouse is hovering over the button
	self.click = false -- whether the mouse has been clicked on the button
	
	self.background = attributes.background or false

	self.debug = attributes.debug or false
end

function Button:update(x,y,dt)	
	local dt = dt or 0
	local mx = x
	local my = y
	if (mx > self.x-self.width/2) and (mx < self.x-self.width/2 + self.width) and (my > self.y-self.height/2) and (my < self.y-self.height/2 + self.height) then
		self.hover = true
	else
		self.hover = false
	end

	if self.label == false then else
		for i,v in ipairs(self.label) do
			self.label[i]:update(dt)
			if self.hover then
				--self.label[i]:setColor()
			end
		end
	end
end

function Button:draw()
	if self.background then
		love.graphics.setColor(unpack(Button.BACKGROUND)) 
		love.graphics.rectangle("fill",self.x-self.width/2, self.y-self.height/2,self.width,self.height)
	end
	if self.debug then 
		love.graphics.setColor(unpack(Button.BACKGROUND)) 
		love.graphics.rectangle("line",self.x-self.width/2, self.y-self.height/2,self.width,self.height)
	end

	if self.label == false then else
		for i,v in ipairs(self.label) do
			self.label[i]:draw()
		end
	end
end

function Button:setPos(x,y)
	self.x = x
	self.y = y
end

function Button:setSize(width,height)
	self.width = width
	self.height = height
end

function Button:setClicked(value)
	self.click = value
end

function Button:getClicked()
	return self.click
end

function Button:mousepressed(x, y, button)
	if self.hover then
		self.click = true
	end	
	return self.click
end

function Button:mousereleased(x,y,button)
	self.click = false
	return self.click
end

return Button
