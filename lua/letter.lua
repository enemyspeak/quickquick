local Letter = class('Letter')

Letter.static.CENTERX = love.graphics.getWidth()/2
Letter.static.CENTERY = love.graphics.getHeight()/2

Letter.static.ATLAS1 = lg.newImage('res/letters1.png')
Letter.static.ATLAS2 = lg.newImage('res/letters2.png')
Letter.static.ATLAS3 = lg.newImage('res/numbers1.png')
Letter.static.ATLAS4 = lg.newImage('res/numbers2.png')
Letter.static.ATLAS5 = lg.newImage('res/ds4icons.png')

Letter.static.QUADS = 	{
						["a"] = lg.newQuad(0,0,67,87,Letter.ATLAS1:getDimensions()),
						["b"] = lg.newQuad(72,0,43,87,Letter.ATLAS1:getDimensions()),
						["c"] = lg.newQuad(116,0,40,87,Letter.ATLAS1:getDimensions()),
						["d"] = lg.newQuad(158,0,42,87,Letter.ATLAS1:getDimensions()),
						["e"] = lg.newQuad(202,0,37,87,Letter.ATLAS1:getDimensions()),
						["f"] = lg.newQuad(240,0,45,87,Letter.ATLAS1:getDimensions()),
						["g"] = lg.newQuad(282,0,44,87,Letter.ATLAS1:getDimensions()),
						["h"] = lg.newQuad(330,0,47,87,Letter.ATLAS1:getDimensions()),
						["i"] = lg.newQuad(378,0,20,87,Letter.ATLAS1:getDimensions()),
						["j"] = lg.newQuad(398,0,47,87,Letter.ATLAS1:getDimensions()),
						["k"] = lg.newQuad(0,87,47,84,Letter.ATLAS1:getDimensions()),
						["l"] = lg.newQuad(48,87,46,84,Letter.ATLAS1:getDimensions()),
						["m"] = lg.newQuad(94,87,78,84,Letter.ATLAS1:getDimensions()),
						["n"] = lg.newQuad(170,87,48,85,Letter.ATLAS1:getDimensions()),
						["o"] = lg.newQuad(220,87,48,84,Letter.ATLAS1:getDimensions()),
						["p"] = lg.newQuad(268,87,46,87,Letter.ATLAS1:getDimensions()),
						["q"] = lg.newQuad(316,87,50,84,Letter.ATLAS1:getDimensions()),
						["r"] = lg.newQuad(376,87,48,84,Letter.ATLAS1:getDimensions()),
						["s"] = lg.newQuad(0,174,44,87,Letter.ATLAS1:getDimensions()),
						["t"] = lg.newQuad(46,174,44,84,Letter.ATLAS1:getDimensions()),
						["u"] = lg.newQuad(94,174,48,84,Letter.ATLAS1:getDimensions()),
						["v"] = lg.newQuad(142,174,44,84,Letter.ATLAS1:getDimensions()),
						["w"] = lg.newQuad(192,174,68,84,Letter.ATLAS1:getDimensions()),
						["x"] = lg.newQuad(258,174,52,87,Letter.ATLAS1:getDimensions()),
						["y"] = lg.newQuad(312,174,44,87,Letter.ATLAS1:getDimensions()),
						["z"] = lg.newQuad(355,174,58,84,Letter.ATLAS1:getDimensions()),
						["@"] = lg.newQuad(426,87,67,87,Letter.ATLAS1:getDimensions()),
						["+"] = lg.newQuad(426,174,67,87,Letter.ATLAS1:getDimensions()),
						["0"] = lg.newQuad(428,0,54,87,Letter.ATLAS3:getDimensions()),
						["1"] = lg.newQuad(10,0,40,87,Letter.ATLAS3:getDimensions()),
						["2"] = lg.newQuad(50,0,47,87,Letter.ATLAS3:getDimensions()),
						["3"] = lg.newQuad(110,0,40,87,Letter.ATLAS3:getDimensions()),
						["4"] = lg.newQuad(154,0,46,87,Letter.ATLAS3:getDimensions()),
						["5"] = lg.newQuad(200,0,45,87,Letter.ATLAS3:getDimensions()),
						["6"] = lg.newQuad(245,0,47,87,Letter.ATLAS3:getDimensions()),
						["7"] = lg.newQuad(294,0,47,87,Letter.ATLAS3:getDimensions()),
						["8"] = lg.newQuad(341,0,44,87,Letter.ATLAS3:getDimensions()),
						["9"] = lg.newQuad(388,0,40,87,Letter.ATLAS3:getDimensions()),
						[" "] = lg.newQuad(999,0,40,87,Letter.ATLAS3:getDimensions()),
						["ds4x"] = 		lg.newQuad(0,0,86,86,Letter.ATLAS5:getDimensions()),
						["ds4s"] = 		lg.newQuad(86,0,86,86,Letter.ATLAS5:getDimensions()),
						["ds4o"] = 		lg.newQuad(86*2,0,86,86,Letter.ATLAS5:getDimensions()),
						["ds4t"] = 		lg.newQuad(86*3,0,86,86,Letter.ATLAS5:getDimensions()),
						["ds4up"] = 	lg.newQuad(86*4,0,86,86,Letter.ATLAS5:getDimensions()),
						["ds4right"] = 	lg.newQuad(86*5,0,86,86,Letter.ATLAS5:getDimensions()),
						["ds4down"] = 	lg.newQuad(86*6,0,86,86,Letter.ATLAS5:getDimensions()),
						["ds4left"] = 	lg.newQuad(86*7,0,86,86,Letter.ATLAS5:getDimensions())
						}

function Letter:initialize(attributes)
	local attributes = attributes or {}
	self.x = attributes.x or 0
	self.y = attributes.y or 0
		
	self.n = attributes.n or "a"

	self.delay = attributes.delay or 0
	self.t = self.delay --1.35
	
	self.lifespan = attributes.lifespan or false

	self.animationFrameSwitcher = true
	self.animationCounter = attributes.animationDelay or self:random(0,1,3)
	
	self.zoom = attributes.zoom
	if attributes.zoom == nil then
		self.zoom = true
	end

	self.ripple = attributes.ripple or false

	self.fade = attributes.fade or false
	self.fadeSpeed = attributes.fadeSpeed or 1
	self.fadeRandom = attributes.fadeRandom or false

	self.color = { n = 1, r = attributes.colorR or 0,g= attributes.colorG or 0,b= attributes.colorB or 0, a = attributes.colorA or 255}
	self.colorTo = { r = attributes.colorToR or 255,g= attributes.colorToG or 255,b= attributes.colorToB or 255, a = attributes.colorToA or 255}

	self.rippleY = 0
	self.amp = attributes.amp or 0.25
	self.speed = attributes.speed or 2

	self.showToggle = false
	self.isFading = false

	self.rotation = attributes.rotation or 0

	self.scale = {scale = attributes.scale or 0}	-- this is the starting scale
	self.scaleTo = attributes.scaleTo or 0.5		-- this is where you're scaling to
	self.scaleSpeed = attributes.scaleSpeed or 0.35 	-- this is how fast you're doing it

	self.animationInterval = self:random(0,1,3)

	self.highlight = false
	self.highlightSin = 0
	self.highlightTime = math.pi/2

	flux.to(self.scale, self.scaleSpeed, { scale = self.scaleTo}):ease("quadout"):delay(self.delay)
	
	self.elastic = attributes.elastic or false
	self.popup = attributes.popup or false
	
	if self.popup then
		self.popupCounter = 0
		local popupDistance = 150
		local popupTime = attributes.popupTime or 1
		local ypos = self.y
		self.y = self.y + popupDistance
		if self.elastic then
			flux.to(self,popupTime,{y = ypos}):ease("backout"):delay(self.delay)
		else
			flux.to(self,popupTime,{y = ypos}):ease("quadout"):delay(self.delay)
		end
	end
	if self.fade then
		flux.to(self.color, self.fadeSpeed, { r=self.colorTo.r,g=self.colorTo.g,b=self.colorTo.b, a = self.colorTo.a }):ease("quadin"):delay(self.fade)
	end

	if self.lifespan == false then else
		if self.fadeRandom then
			flux.to(self.color, self.fadeSpeed*10, {a = 0}):ease("quadin"):delay(self.lifespan+self:random(0,0.5,3))
		else
			flux.to(self.color, self.fadeSpeed*10, {a = 0}):ease("quadin"):delay(self.lifespan)
		end
	end
end

function Letter:random(min, max, precision)
   local precision = precision or 0
   local num = math.random()
   local range = math.abs(max - min)
   local offset = range * num
   local randomnum = min + offset
   return math.floor(randomnum * math.pow(10, precision) + 0.5) / math.pow(10, precision)
end

function Letter:setPosition(x,y)
	self.x = x
	self.y = y
end

function Letter:startFade(d, value)
	if self.isFading then else
		local d = d
		local v = value
		self.isFading = true
		if value == nil then value = self.fadeSpeed*10 end
		if d == nil then d = 0 end
		if self.fadeRandom then
			flux.to(self.color, value, {a = 0}):ease("quadin"):delay(d+self:random(0,0.5,3))
		else
			flux.to(self.color, value, {a = 0}):ease("quadin"):delay(d)
		end
	end
end

function Letter:setScale( value )
	self.scaleSpeed = 0.5
	flux.to(self.scale, self.scaleSpeed, { scale = value}):ease("quadinout"):delay(self.delay)
end

function Letter:setN( value )
	self.n = value
end

function Letter:setColor(r,g,b,a)
	self.color.r = r
	self.color.g = g
	self.color.b = b
	if a == nil then else
		self.color.a = a
	end
end

function Letter:setHighlight(v)
	self.highlight = v
end

function Letter:update(dt, xpercent,ypercent)	
	local xratio = 0
	local yratio = 1

	if xpercent == nil then else
		xratio = xpercent
		yratio = ypercent
	end	

	if self.highlight then
		self.highlightTime = self.highlightTime + dt*4
		if self.highlightTime > math.pi*2 then self.highlightTime = 0 end
		self.highlightSin = math.abs(math.sin(self.highlightTime))
	end

	if self.ripple then
		self.t = self.t + dt
		self.rippleY = (math.sin((self.t+self.delay)*self.speed)*self.amp*yratio)
	end
	
	self.animationCounter = self.animationCounter + dt
	if self.animationCounter > self.animationInterval then
		self.animationFrameSwitcher = not self.animationFrameSwitcher
		self.animationCounter = 0
		self.animationInterval = self:random(0.5,1.75,2)
	end
end

function Letter:draw(value)
	if self.highlight then
		lg.setColor(self.color.r,self.color.g*self.highlightSin,self.color.b*self.highlightSin,self.color.a)
	else
		lg.setColor(self.color.r,self.color.g,self.color.b,self.color.a)
	end

	local scale = self.scale.scale
	local quad = Letter.QUADS[self.n]
	local x, y, w, h = quad:getViewport()
	if string.match(self.n,'ds4') ~= nil then
		lg.draw(Letter.ATLAS5,quad,self.x,self.y + self.rippleY,self.rotation,scale,scale,w/2,h/2)	
	elseif tonumber(self.n) == nil then
		if self.animationFrameSwitcher then
			lg.draw(Letter.ATLAS1,quad,self.x,self.y + self.rippleY,self.rotation,scale,scale,w/2,h/2)
		else
			lg.draw(Letter.ATLAS2,quad,self.x,self.y + self.rippleY,self.rotation,scale,scale,w/2,h/2)
		end
	else
		if self.animationFrameSwitcher then
			lg.draw(Letter.ATLAS3,quad,self.x,self.y + self.rippleY,self.rotation,scale,scale,w/2,h/2)
		else
			lg.draw(Letter.ATLAS4,quad,self.x,self.y + self.rippleY,self.rotation,scale,scale,w/2,h/2)
		end
	end
end

return Letter
