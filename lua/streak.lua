		
local Streak = class('Streak')

Streak.static.COLOR1 = {100,88,160}
Streak.static.WIDTH = 500

Streak.static.CENTERX = love.graphics.getWidth()/2
Streak.static.CENTERY = love.graphics.getHeight()/2

function Streak:initialize(attributes)
	local attributes = attributes or {}
	self.x = attributes.x or Streak.CENTERX + Streak.SAFEZONE
	self.y = attributes.y or Streak.CENTERY + Streak.SAFEZONE
	self.levelscale = attributes.scale or 1
	self.speed = attributes.speed or 1 + (math.random(0,100)/100)*4
	self.width = 0.25 + (math.random(0,100)/100)*2

	self.xpos = self.x
	self.ypos = self.y

	self.color = attributes.color or Streak.COLOR1
	self.alpha = 255 - (self.width*4)
	self.finished = false

	self.delay = (math.random(0,100)/100)

	local complete = function() self.finished = true end
	flux.to(self,self.speed,{x = -Streak.WIDTH}):ease("quadinout")
	flux.to(self,self.speed,{xpos = -Streak.WIDTH}):ease("quadinout"):delay(self.delay):oncomplete(complete)
end

function Streak:update(dt)	

end

function Streak:getKill()
	return self.finished
end

function Streak:draw(value)
	local w = lg.getLineWidth()
	lg.setLineWidth(self.width)
	local r,g,b = unpack(Streak.COLOR1)
	love.graphics.setColor(r,g,b,self.alpha)
	lg.line(self.x,self.y,self.xpos,self.ypos)
--	lg.circle("fill",self.x,self.y,self.width-2)
--	lg.circle("fill",self.xpos,self.ypos,self.width-2)
	lg.setLineWidth(w)
end

return Streak
