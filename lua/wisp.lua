		
local Wisp = class('Wisp')

Wisp.static.COLOR1 = {66,58,60}

Wisp.static.CENTERX = love.graphics.getWidth()
Wisp.static.CENTERY = love.graphics.getHeight()

Wisp.static.SAFEZONE = 50
Wisp.static.KILLZONE = 50

function Wisp:initialize(attributes)
	local attributes = attributes or {}
	self.x = attributes.x or 0
	self.y = attributes.y or 0

	self.levelscale = attributes.scale or 1
	self.scale = 1

	self.baseSpeed = -attributes.speed or -1	
	self.speed = self.baseSpeed
	self.basePitch = attributes.pitch or 0
	self.pitch = self.basePitch
	self.offset = 1

	self.quad = attributes.quad or math.random(1,8)
	self.size = attributes.size or math.random(1,3)
	
	self.looping = false

	self.hasTrail = true
	self.trail = {}

	self.color = attributes.color or Wisp.COLOR1
	self.alpha = 255
	self.finished = false

	self.trailEnd = 1
	self.trailSpeed = 12
	self.trailLength = 100
	self.trail[1] = {self.x,self.y}
	local pitchy = 1
	for i = 2, self.trailLength do
		self.speed = self.baseSpeed - math.sin(i) - 2
		self.pitch = self.basePitch + math.cos(i*0.5)*4 
									+ math.cos(i*2)*2
									+ math.cos(1.5 + i * 0.01)*10 

		pitchy = pitchy + 0.05 + math.sin(i)

		if i > self.trailLength/3 then
		end

		local y = self.trail[i-1][2] + (self.speed * pitchy)
		local x = self.trail[i-1][1] + (self.speed * self.pitch)
		
		self.trail[i] = {x,y}
	end

	flux.to(self,self.trailSpeed, {trailEnd = 100}):ease("quadout")
end

function Wisp:setLevelComplete()
	flux.to(self,0.5,{alpha = 0}):ease("quadin")
end

function Wisp:setWind(n)
	self.pitch = n
end

function Wisp:update(dt)
	for i,v in ipairs(self.trail) do
	--	self.trail[i][1] = self.trail[i][1] * 0.998
	end
end

function Wisp:fadeOut( d )
	local d = d or 0
	flux.to(self,1,{alpha=0}):ease("quadout"):delay(d)
end

function Wisp:getKill()
	return self.finished
end

function Wisp:draw(value)
	local r,g,b,a
	local mode = love.graphics.getBlendMode( )
	lg.setBlendMode("screen")
	r,g,b = unpack(Wisp.COLOR1)
	lg.setLineWidth(self.size)
	for i = 2, self.trailEnd do
		a = 10 * math.abs(i-self.trailEnd)
		if a > 255 then a = 255 end
		if a < 0 then a = 0 end
		lg.setColor(r,g,b,a)
		lg.line(self.trail[i][1],self.trail[i][2],self.trail[i-1][1],self.trail[i-1][2])
	end
	lg.setBlendMode(mode)
end

return Wisp
