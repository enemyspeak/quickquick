		
local Particle = class('Particle')

Particle.static.COLOR1 = {92,76,77}
Particle.static.COLOR2 = {225,110,85}

Particle.static.ATLAS = lg.newImage('res/snow.png')
Particle.static.QUADS = {
						[1] = lg.newQuad(0, 	0, 	16, 	16, Particle.ATLAS:getDimensions()),
						[2] = lg.newQuad(16*1, 	0, 	16, 	16, Particle.ATLAS:getDimensions()),
						[3] = lg.newQuad(16*2, 	0, 	16, 	16, Particle.ATLAS:getDimensions()),
						[4] = lg.newQuad(16*3, 	0, 	16, 	16, Particle.ATLAS:getDimensions()),
						[5] = lg.newQuad(16*4, 	0, 	16, 	16, Particle.ATLAS:getDimensions()),
						[6] = lg.newQuad(16*5, 	0, 	16, 	16, Particle.ATLAS:getDimensions()),
						[7] = lg.newQuad(16*6, 	0, 	16, 	16, Particle.ATLAS:getDimensions()),
						[8] = lg.newQuad(16*7, 	0, 	16, 	16, Particle.ATLAS:getDimensions()),
						}

Particle.static.CENTERX = love.graphics.getWidth()
Particle.static.CENTERY = love.graphics.getHeight()
Particle.static.SAFEZONE = 50
Particle.static.FADEZONE = 100
Particle.static.KILLZONE = 50
Particle.static.TRAILLENGTH = 10
Particle.static.EMBERLENGTH = 4
Particle.static.WINDDECAY = 40
Particle.static.WINDCOOLDOWN = 1

function Particle:initialize(attributes)
	local attributes = attributes or {}
	self.x = attributes.x or 0
	self.y = attributes.y or 0

	self.levelscale = attributes.scale or 1
	self.scale = 1

	self.baseSpeed = attributes.speed or 1	
	self.speed = self.baseSpeed
	self.basePitch = attributes.pitch or 0
	self.pitch = self.basePitch
	self.offset = math.random() + math.random(0,10)

	self.quad = attributes.quad or math.random(1,8)
	self.size = attributes.size or math.random(0,2)+math.random()+math.random()
	
	self.ember = false
	if self.ember then
		self.speed = -self.speed*2
		self.baseSpeed = -self.baseSpeed*8
	end

	self.looping = true

	self.hasTrail = true
	self.trail = {}

	self.wind = {0,0}
	self.windCooldownTime = 10

	self.accumulator = self.offset + math.random()

	self.color = attributes.color or Particle.COLOR1
	self.alpha = 255
	self.finished = false
end

function Particle:setWind(angle,intensity)
	self.windCooldownTime = 0
	angle = angle + math.pi/2
	
	flux.to(self.wind,1,{math.cos(angle)*intensity,math.sin(angle)*intensity}):ease("quadinout")
end

function Particle:update(dt)
	self.windCooldownTime = self.windCooldownTime + dt
	self.accumulator = self.accumulator + dt
	if self.ember then
		self.speed = self.baseSpeed + math.sin(self.accumulator)*10
		self.pitch = self.basePitch + math.cos(self.accumulator) + math.sin(self.accumulator * 0.3)
	else
		self.speed = self.baseSpeed + math.sin(self.accumulator)*10
		self.pitch = self.basePitch + math.cos(self.accumulator) + math.sin(self.accumulator * 0.3)
	end

	self.x = self.x + (self.speed * self.pitch + self.offset + self.wind[1])*dt
	self.y = self.y + (self.speed + self.wind[2]) * dt


	--print(math.sin(self.accumulator*(self.wind[2]/10))*self.wind[2])

	if self.looping then
		if self.y > (Particle.CENTERY - Particle.FADEZONE) --or self.y < Particle.FADEZONE
		or self.x > (Particle.CENTERX - Particle.FADEZONE) or self.x < Particle.FADEZONE then
		--	self:fadeOut()
		else 
		--	self:fadeIn()
		end
		
		if self.y > Particle.CENTERY + Particle.KILLZONE then
			self.y = -Particle.KILLZONE+1
			self.trail = {}
			self.wind = {0,0}
		end
		if self.x > Particle.CENTERX + Particle.KILLZONE then
			self.x = -Particle.KILLZONE+1
			self.trail = {}
			self.wind = {0,0}
		end
		if self.y < -Particle.KILLZONE then
			self.y = Particle.CENTERY+Particle.KILLZONE-1
			self.trail = {}
			self.wind = {0,0}
		end
		if self.x < -Particle.KILLZONE then
			self.x = Particle.CENTERX+Particle.KILLZONE-1
			self.trail = {}
			self.wind = {0,0}
		end
	else
		local kill = ((Particle.CENTERY)/self.levelscale) + Particle.KILLZONE
		if self.y < -kill then
			self.finished = true
		end
	end
	if self.hasTrail then
		table.insert(self.trail,{self.x,self.y})
		local length = Particle.TRAILLENGTH
		if self.ember then
			length = Particle.EMBERLENGTH
		end
		if #self.trail > length then
			table.remove(self.trail,1)
		end
	end
end

function Particle:fadeIn( d )
	local d = d or 0
	flux.to(self,1,{alpha=255}):ease("quadout"):delay(d)
end

function Particle:fadeOut( d )
	local d = d or 0
	flux.to(self,1,{alpha=0}):ease("quadout"):delay(d)
end

function Particle:getPosition()
	return self.x,self.y
end

function Particle:getKill()
	return self.finished
end

function Particle:draw(value)
	local r,g,b
	if self.ember then
		r,g,b = unpack(Particle.COLOR2)
	else
		r,g,b = unpack(Particle.COLOR1)
	end
	love.graphics.setColor(r,g,b,self.alpha)
	if self.hasTrail then
		lg.setLineWidth(self.size)

		lg.circle("fill",self.trail[#self.trail][1],self.trail[#self.trail][2],self.size/2)
		lg.circle("fill",self.trail[1][1],self.trail[1][2],self.size/2)
		
		for i = 2, #self.trail do
			lg.line(self.trail[i][1],self.trail[i][2],self.trail[i-1][1],self.trail[i-1][2])
		end
	else
		lg.draw(Particle.ATLAS,Particle.QUADS[self.quad],0.5+math.floor(self.x),0.5+math.floor(self.y),self.pitch/1000,self.scale,self.scale)
	end
end

return Particle
