local Dualshock = class('Dualshock')

Dualshock.static.BASE = love.graphics.newImage("res/ds4base.png")
Dualshock.static.BUTTONS = lg.newImage("res/ds4buttons.png")
Dualshock.static.LABELS = lg.newImage("res/ds4labels.png")
Dualshock.static.LETTERS = require ('lua.letter')

Dualshock.static.QUADS = 	{
							shoulder1 = lg.newQuad(80,0,80,64,Dualshock.BUTTONS:getDimensions()),
							shoulder2 = lg.newQuad(0,0,80,64,Dualshock.BUTTONS:getDimensions()),
							button = lg.newQuad(80*2,0,80,64,Dualshock.BUTTONS:getDimensions()),
							opt = lg.newQuad(80*3,0,80,64,Dualshock.BUTTONS:getDimensions()),
							pad = lg.newQuad(80*4,0,80,64,Dualshock.BUTTONS:getDimensions()),
							padfill = lg.newQuad(80*5,0,80,64,Dualshock.BUTTONS:getDimensions()),
							optfill = lg.newQuad(80*6,0,80,64,Dualshock.BUTTONS:getDimensions()),
							shoulderfill = lg.newQuad(80*7,0,80,64,Dualshock.BUTTONS:getDimensions())
							}
Dualshock.static.QUADS2 = 	{
							square = lg.newQuad(0,0,20,20,Dualshock.LABELS:getDimensions()),
							triangle = lg.newQuad(20,0,20,20,Dualshock.LABELS:getDimensions()),
							circle = lg.newQuad(40,0,20,20,Dualshock.LABELS:getDimensions()),
							cross = lg.newQuad(60,0,20,20,Dualshock.LABELS:getDimensions())
							}

Dualshock.static.SCALE = 0.5

Dualshock.static.LINES = {49,130,138}
Dualshock.static.PUSHED = {174,204,184}
Dualshock.static.SLOTWIDTH = 300
Dualshock.static.SLOTHEIGHT = 350
Dualshock.static.SLOTCOOLDOWN = 0.25
Dualshock.static.POSITIONS = {
							[1] = Dualshock.SLOTWIDTH/Dualshock.SCALE,
							[2] = (1280/2)/Dualshock.SCALE,
							[3] = (1280-Dualshock.SLOTWIDTH)/Dualshock.SCALE
}

function Dualshock:initialize(attributes)
	local attributes = attributes or {}
	self.x = attributes.x or 0
	self.y = attributes.y or 0

	self.input = attributes.input

	self.deadzone = 0.4

	self.lstickx = 0
	self.lsticky = 0
	self.rstickx = 0
	self.rsticky = 0

	self.slot = attributes.slot or 2
	self.yslot = 1

	self.x = Dualshock.POSITIONS[self.slot]
	self.y = Dualshock.POSITIONS[self.yslot]
	self.label = "Player "..self.yslot
	
	self.slotCooldown = 0
end

function Dualshock:setY(value)
	self.yslot = value
	self.y = Dualshock.POSITIONS[self.yslot]
end

function Dualshock:update(dt)
	if self.slotCooldown > 0 then else
		if self.input:isGamepadDown("dpleft") or self.input:getGamepadAxis("leftx") < -self.deadzone  then
			self.slot = self.slot - 1 
			if self.slot < 1 then self.slot = 1 end
			flux.to(self,Dualshock.SLOTCOOLDOWN,{x = Dualshock.POSITIONS[self.slot]}):ease("quadout")
			self.slotCooldown = Dualshock.SLOTCOOLDOWN
		elseif self.input:isGamepadDown("dpright") or self.input:getGamepadAxis("leftx") > self.deadzone then
			self.slot = self.slot + 1
			if self.slot > 3 then self.slot = 3 end -- >3 ! 
			flux.to(self,Dualshock.SLOTCOOLDOWN,{x = Dualshock.POSITIONS[self.slot]}):ease("quadout")
			self.slotCooldown = Dualshock.SLOTCOOLDOWN
		end
	end
	self.slotCooldown = self.slotCooldown - dt	
end

function Dualshock:draw()	
	lg.push()
	lg.scale(Dualshock.SCALE)

	lg.setColor(Dualshock.LINES)
	lg.draw(Dualshock.BASE,self.x,self.y,0,1,1,480/2,310/2)	-- base

	local x, y, w, h
	local quad
	local quad2

	-- sticks	
		lg.setColor(Dualshock.LINES)
		quad = Dualshock.QUADS["button"]
		x, y, w, h = quad:getViewport()
		local stickoffsetx = 70
		local stickoffsety = 4
		local stickmultiplier = 22
		local buttonsize = 19
		lg.circle("fill",self.x-stickoffsetx+self.input:getGamepadAxis("leftx")*stickmultiplier,self.y+stickoffsety+self.input:getGamepadAxis("lefty")*stickmultiplier,buttonsize)
		lg.draw(Dualshock.BUTTONS,quad,self.x-stickoffsetx+self.input:getGamepadAxis("leftx")*stickmultiplier,self.y+stickoffsety+self.input:getGamepadAxis("lefty")*stickmultiplier,0,1,1,80/2,64/2)
		lg.circle("fill",self.x+stickoffsetx+self.input:getGamepadAxis("rightx")*stickmultiplier,self.y+stickoffsety+self.input:getGamepadAxis("righty")*stickmultiplier,buttonsize)
		lg.draw(Dualshock.BUTTONS,quad,self.x+stickoffsetx+self.input:getGamepadAxis("rightx")*stickmultiplier,self.y+stickoffsety+self.input:getGamepadAxis("righty")*stickmultiplier,0,1,1,80/2,64/2)
		
	-- face buttons
		local faceoffsetx = 138
		local facebuttonoffset = 30
		local faceoffsety = 75
		quad2 = Dualshock.QUADS2["cross"]
		if self.input:isGamepadDown("a") then
			lg.setColor(Dualshock.LINES)
			lg.circle("fill",self.x+faceoffsetx,self.y-faceoffsety+facebuttonoffset,buttonsize)
			lg.setColor(Dualshock.PUSHED)
		else
			lg.setColor(Dualshock.LINES)
		end
			lg.draw(Dualshock.LABELS,quad2,self.x+faceoffsetx,self.y-faceoffsety+facebuttonoffset,0,1,1,20/2,20/2)
			lg.draw(Dualshock.BUTTONS,quad,self.x+faceoffsetx,self.y-faceoffsety+facebuttonoffset,0,1,1,80/2,64/2)
		quad2 = Dualshock.QUADS2["circle"]
		if self.input:isGamepadDown("b") then
			lg.setColor(Dualshock.LINES)
			lg.circle("fill",self.x+faceoffsetx+facebuttonoffset,self.y-faceoffsety,buttonsize)
			lg.setColor(Dualshock.PUSHED)
		else
			lg.setColor(Dualshock.LINES)
		end
			lg.draw(Dualshock.LABELS,quad2,self.x+faceoffsetx+facebuttonoffset,self.y-faceoffsety,0,1,1,20/2,20/2)
			lg.draw(Dualshock.BUTTONS,quad,self.x+faceoffsetx+facebuttonoffset,self.y-faceoffsety,0,1,1,80/2,64/2)

		quad2 = Dualshock.QUADS2["square"]
		if self.input:isGamepadDown("x") then
			lg.setColor(Dualshock.LINES)
			lg.circle("fill",self.x+faceoffsetx-facebuttonoffset,self.y-faceoffsety,buttonsize)
			lg.setColor(Dualshock.PUSHED)			
		else
			lg.setColor(Dualshock.LINES)
		end
			lg.draw(Dualshock.LABELS,quad2,self.x+faceoffsetx-facebuttonoffset,self.y-faceoffsety,0,1,1,20/2,20/2)
			lg.draw(Dualshock.BUTTONS,quad,self.x+faceoffsetx-facebuttonoffset,self.y-faceoffsety,0,1,1,80/2,64/2)
		quad2 = Dualshock.QUADS2["triangle"]
		if self.input:isGamepadDown("y") then
			lg.setColor(Dualshock.LINES)
			lg.circle("fill",self.x+faceoffsetx,self.y-faceoffsety-facebuttonoffset,buttonsize)
			lg.setColor(Dualshock.PUSHED)
		else
			lg.setColor(Dualshock.LINES)
		end
		lg.draw(Dualshock.LABELS,quad2,self.x+faceoffsetx,self.y-faceoffsety-facebuttonoffset,0,1,1,20/2,20/2)
		lg.draw(Dualshock.BUTTONS,quad,self.x+faceoffsetx,self.y-faceoffsety-facebuttonoffset,0,1,1,80/2,64/2)
	-- guide
		local guideoffset = 20
		if self.input:isGamepadDown("guide") then
			lg.setColor(Dualshock.LINES)
			lg.circle("fill",self.x,self.y-guideoffset,buttonsize)
			lg.setColor(Dualshock.PUSHED)
		else
			lg.setColor(Dualshock.LINES)
		end
		lg.draw(Dualshock.BUTTONS,quad,self.x,self.y-guideoffset,0,1,1,80/2,64/2)
	-- dpad
		facebuttonoffset = facebuttonoffset - 5

		if self.input:isGamepadDown("dpup") then
			lg.setColor(Dualshock.LINES)
			quad = Dualshock.QUADS["padfill"]
			lg.draw(Dualshock.BUTTONS,quad,self.x-faceoffsetx,self.y-faceoffsety-facebuttonoffset,math.rad(-90),1,1,80/2,64/2)
			lg.setColor(Dualshock.PUSHED)
		else
			lg.setColor(Dualshock.LINES)
		end	
		quad = Dualshock.QUADS["pad"]
		lg.draw(Dualshock.BUTTONS,quad,self.x-faceoffsetx,self.y-faceoffsety-facebuttonoffset,math.rad(-90),1,1,80/2,64/2)
		
		if self.input:isGamepadDown("dpdown") then
			quad = Dualshock.QUADS["padfill"]
			lg.setColor(Dualshock.LINES)
			lg.draw(Dualshock.BUTTONS,quad,self.x-faceoffsetx,self.y-faceoffsety+facebuttonoffset,math.rad(90),1,1,80/2,64/2)
			lg.setColor(Dualshock.PUSHED)
		else
			lg.setColor(Dualshock.LINES)
		end	
		quad = Dualshock.QUADS["pad"]
		lg.draw(Dualshock.BUTTONS,quad,self.x-faceoffsetx,self.y-faceoffsety+facebuttonoffset,math.rad(90),1,1,80/2,64/2)

		
		if self.input:isGamepadDown("dpright") then
			quad = Dualshock.QUADS["padfill"]
			lg.setColor(Dualshock.LINES)
			lg.draw(Dualshock.BUTTONS,quad,self.x-faceoffsetx+facebuttonoffset,self.y-faceoffsety,0,1,1,80/2,64/2)
			lg.setColor(Dualshock.PUSHED)
		else
			lg.setColor(Dualshock.LINES)
		end	
		quad = Dualshock.QUADS["pad"]
		lg.draw(Dualshock.BUTTONS,quad,self.x-faceoffsetx+facebuttonoffset,self.y-faceoffsety,0,1,1,80/2,64/2)

		if self.input:isGamepadDown("dpleft") then
			quad = Dualshock.QUADS["padfill"]
			lg.setColor(Dualshock.LINES)
			lg.draw(Dualshock.BUTTONS,quad,self.x-faceoffsetx-facebuttonoffset,self.y-faceoffsety,math.rad(-180),1,1,80/2,64/2)
			lg.setColor(Dualshock.PUSHED)
		else
			lg.setColor(Dualshock.LINES)
		end	
		quad = Dualshock.QUADS["pad"]
		lg.draw(Dualshock.BUTTONS,quad,self.x-faceoffsetx-facebuttonoffset,self.y-faceoffsety,math.rad(-180),1,1,80/2,64/2)
	-- trigger 1
		local shoulderoffsetx = 137
		local shoulderoffsety = 155
		if self.input:isGamepadDown("leftshoulder") then
			lg.setColor(Dualshock.LINES)
			quad = Dualshock.QUADS["shoulderfill"]
			lg.draw(Dualshock.BUTTONS,quad,self.x-shoulderoffsetx,self.y-shoulderoffsety,0,1,1,80/2,64/2)
			lg.setColor(Dualshock.PUSHED)
		else
			lg.setColor(Dualshock.LINES)
		end
		quad = Dualshock.QUADS["shoulder1"]
		lg.draw(Dualshock.BUTTONS,quad,self.x-shoulderoffsetx,self.y-shoulderoffsety,0,1,1,80/2,64/2)
		if self.input:isGamepadDown("rightshoulder") then
			lg.setColor(Dualshock.LINES)
			quad = Dualshock.QUADS["shoulderfill"]
			lg.draw(Dualshock.BUTTONS,quad,self.x+shoulderoffsetx,self.y-shoulderoffsety,0,1,1,80/2,64/2)
			lg.setColor(Dualshock.PUSHED)
		else
			lg.setColor(Dualshock.LINES)
		end
		quad = Dualshock.QUADS["shoulder1"]
		lg.draw(Dualshock.BUTTONS,quad,self.x+shoulderoffsetx,self.y-shoulderoffsety,0,1,1,80/2,64/2)
	--	trigger 2		
		shoulderoffsety = shoulderoffsety + 40
		local triggermultiplier = 40
		local shoulderwidth = 60
		local shoulderheight = 40

		local deadzone = 0.1
		quad = Dualshock.QUADS["shoulder2"]
		lg.setColor(Dualshock.LINES)
		lg.rectangle("fill",self.x-shoulderoffsetx-shoulderwidth/2,self.y-shoulderoffsety+shoulderheight/2,shoulderwidth,-self.input:getGamepadAxis("triggerleft")*triggermultiplier)
		if self.input:getGamepadAxis("triggerleft") < deadzone then lg.setColor(Dualshock.LINES) else lg.setColor(Dualshock.PUSHED) end
		lg.draw(Dualshock.BUTTONS,quad,self.x-shoulderoffsetx,self.y-shoulderoffsety,0,1,1,80/2,64/2)
		lg.setColor(Dualshock.LINES)	
		lg.rectangle("fill",self.x+shoulderoffsetx-shoulderwidth/2,self.y-shoulderoffsety+shoulderheight/2,shoulderwidth,-self.input:getGamepadAxis("triggerright")*triggermultiplier)
		if self.input:getGamepadAxis("triggerright") < deadzone then lg.setColor(Dualshock.LINES) else lg.setColor(Dualshock.PUSHED) end
		lg.draw(Dualshock.BUTTONS,quad,self.x+shoulderoffsetx,self.y-shoulderoffsety,0,1,1,80/2,64/2)
	-- options
		local optoffsetx = 75
		local optoffsety = 121
		if self.input:isGamepadDown("start") then
			quad = Dualshock.QUADS["optfill"]
			lg.setColor(Dualshock.LINES)	
			lg.draw(Dualshock.BUTTONS,quad,self.x+optoffsetx,self.y-optoffsety,0,1,1,80/2,64/2)
			lg.setColor(Dualshock.PUSHED)	
		else
			lg.setColor(Dualshock.LINES)		
		end
		quad = Dualshock.QUADS["opt"]
		lg.draw(Dualshock.BUTTONS,quad,self.x+optoffsetx,self.y-optoffsety,0,1,1,80/2,64/2)
		if self.input:isGamepadDown("back") then
			quad = Dualshock.QUADS["optfill"]
			lg.setColor(Dualshock.LINES)	
			lg.draw(Dualshock.BUTTONS,quad,self.x-optoffsetx,self.y-optoffsety,0,1,1,80/2,64/2)
			lg.setColor(Dualshock.PUSHED)	
		else
			lg.setColor(Dualshock.LINES)		
		end
		quad = Dualshock.QUADS["opt"]
		lg.draw(Dualshock.BUTTONS,quad,self.x-optoffsetx,self.y-optoffsety,0,1,1,80/2,64/2)
	-- trackpad?

	lg.pop()
end



return Dualshock
