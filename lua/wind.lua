		
local Wind = class('Wind')

Wind.static.COLOR1 = {92,76,77}

Wind.static.ARROWTEST = lg.newImage('res/arrow.png')

Wind.static.CENTERX = love.graphics.getWidth()
Wind.static.CENTERY = love.graphics.getHeight()
Wind.static.DECAYTIME = 40
Wind.static.CUTOFF = 50
Wind.static.INTENSITYLIMIT = 225
Wind.static.COOLDOWN = 1

function Wind:initialize(attributes)
	local attributes = attributes or {}
	self.positions = 	{}
	self.alpha = 255

	self.lastTime = 0
	self.lastPosition = {0,0}

end

function Wind:addPosition(xpos,ypos,intensity,radius)
	local temp = false
	if (xpos - self.lastPosition[1])^2 + ( ypos - self.lastPosition[2])^2 < Wind.CUTOFF^2 then
		temp = true
	end
	if temp then else
		local a = math.atan2(self.lastPosition[2]-ypos,self.lastPosition[1]-xpos) + math.pi/2
		self.lastPosition[1] = xpos
		self.lastPosition[2] = ypos
		local intensity = intensity or 20/self.lastTime
		if intensity > Wind.INTENSITYLIMIT then 
			intensity = Wind.INTENSITYLIMIT 
		end

		self.positions[#self.positions+1] = {
											xpos,
											ypos,
											intensity,
											intensity,
											a
											}
		self.lastTime = 0
	end
end

function Wind:testPoint(xpos,ypos)
	local temp = false
	local angle = 0
	local intensity = 0
	local x,y,r
	for i,v in ipairs(self.positions) do
		x = self.positions[i][1]
		y = self.positions[i][2]
		r = self.positions[i][4]
		
		if (xpos - x)^2 + ( ypos - y)^2 < r^2 then
			temp = true
			local d = (xpos-x)^2 + (ypos-y)^2
			local ratio = 1 - d/(r^2)
			if intensity < self.positions[i][3]*ratio then
				intensity = self.positions[i][3]*ratio
				angle = self.positions[i][5]
			end
		end
	end

	return temp, angle, intensity
end

function Wind:update(dt)
	self.lastTime = self.lastTime + dt

	for i,v in ipairs(self.positions) do
		self.positions[i][3] = self.positions[i][3] - Wind.DECAYTIME*dt
		if self.positions[i][3] < 0 then
			table.remove(self.positions,i)			
			break
		end
	end
end

function Wind:draw()
	local r,g,b = unpack(Wind.COLOR1)
	local alpha
	lg.setLineWidth(1)
	love.graphics.setColor(r,g,b,255)
	lg.circle("line",self.lastPosition[1],self.lastPosition[2],Wind.CUTOFF)
	
	for i,v in ipairs(self.positions) do
		love.graphics.setColor(r,g,b,self.positions[i][3])
		lg.draw(Wind.ARROWTEST,self.positions[i][1],self.positions[i][2],self.positions[i][5])
		lg.circle("line",self.positions[i][1],self.positions[i][2],self.positions[i][4])
	end
end

return Wind
