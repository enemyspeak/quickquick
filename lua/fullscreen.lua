local doFullscreen = Gamestate:addState('doFullscreen')

local Button = require 'lua.button'
local Letter = require 'lua.letter'

local WIDTH = lg.getWidth()
local HEIGHT = lg.getHeight()
local CENTERX = WIDTH/2
local CENTERY = HEIGHT/2

local buttons = {}

local letters = {}

local debug_text = {}

local outFade = { value = 0 }

local function debug(input, indent)
	if (input == nil) then
		input = "--------"
	end

	if (indent == nil) then
		indent = 0
	end

	local temp_string = tostring(input)

	for i = 1, indent, 1 do
		temp_string = "   " .. temp_string
	end

	table.insert(debug_text, temp_string);
end

local function updateLetters(dt)
	for i,v in ipairs(letters) do
		letters[i]:update(dt)
	end
end

local function updateButtons(dt)
	local mx,my = love.mouse.getPosition()
	mx = mx - CENTERX
	my = my - CENTERY
	for i,v in ipairs(buttons) do
		buttons[i]:update(mx,my,dt)
	end

	if buttons[1]:getClicked() then -- good job you wrote some shitty code here buddy
		local fullscreen = not love.window.getFullscreen()
		if fullscreen then
			local width, height = love.window.getDesktopDimensions( display )
			love.window.setMode( width, height,{fullscreen=true})
			love.resize( width, height )

			WIDTH = lg.getWidth()
			HEIGHT = lg.getHeight()
			CENTERX = WIDTH/2
			CENTERY = HEIGHT/2
			fadeOut(1)
		end
	elseif buttons[2]:getClicked() then -- here too, but not as much
		fadeOut(0)
	end
end

function fadeOut( time)
	local fade = function ()
		gamestate:gotoState("Intro")
	end
	local fadeDelay = time or 0
	flux.to(outFade,0.75, { value = 255}):ease("quadinout"):delay(fadeDelay):oncomplete(fade)
end

local function drawdoFullscreen()
	love.graphics.setColor(0,0,0)	
	for i,v in ipairs(letters) do
		letters[i]:draw()
	end
end

local function drawButtons()
	for i,v in ipairs(buttons) do
		buttons[i]:draw()
	end
end

local function drawFade()
	lg.setColor(255,255,255, outFade.value)
	lg.rectangle("fill",-CENTERX,-CENTERY,WIDTH,HEIGHT)
end 

local function goFullscreen()
	local fullscreen = not love.window.getFullscreen()
	if fullscreen then
		local width, height = love.window.getDesktopDimensions( display )
		width = 1280
		height = 720
		love.window.setMode( width, height,{fullscreen=true})
		love.resize( width, height )

		WIDTH = lg.getWidth()
		HEIGHT = lg.getHeight()
		CENTERX = WIDTH/2
		CENTERY = HEIGHT/2

	    gamestate:gotoState("Intro")
	end
end

local function dontGoFullscreen() -- ;_;
	gamestate:gotoState("Intro")
end




--------

function doFullscreen:enteredState()
	local buttonLabel = {}
	table.insert(buttonLabel,Letter:new({ripple = false, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 1.25,scaleTo = 0.75, fadeSpeed = 0.1, colorR= 0,colorG = 0, colorB = 0, colorA = 0, colorToR= 0,colorToG = 0, colorToB = 0,fade= 1.8, delay= 1.8,x = -225,y=200,qx= 312,qy = 174,qw = 44,qh = 87})) 	-- Y
	table.insert(buttonLabel,Letter:new({ripple = false, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 1.25,scaleTo = 0.75, fadeSpeed = 0.1, colorR= 0,colorG = 0, colorB = 0, colorA = 0, colorToR= 0,colorToG = 0, colorToB = 0,fade= 1.85, delay= 1.85,x = -197,y=198,qx= 202,qw = 37,qh = 87})) 		-- E
	table.insert(buttonLabel,Letter:new({ripple = false, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 1.25,scaleTo = 0.75, fadeSpeed = 0.1, colorR= 0,colorG = 0, colorB = 0, colorA = 0, colorToR= 0,colorToG = 0, colorToB = 0,fade= 1.9, delay= 1.9,x = -172,y=200,qx=0,qy=174,qw = 44,qh = 87})) 		-- S

	table.insert(buttons,Button:new({debug = false, x = -200, y = 200, background = false, width = 125, height = 100, label = buttonLabel}))

	buttonLabel = {}
	table.insert(buttonLabel,Letter:new({ripple = false, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 1.25,scaleTo = 0.75, fadeSpeed = 0.1, colorR= 0,colorG = 0, colorB = 0, colorA = 0, colorToR= 0,colorToG = 0, colorToB = 0,fade= 2.2, delay= 2.2,x = 190,y=200,qx= 170,qy = 87,qw = 44,qh = 84})) 	-- N
	table.insert(buttonLabel,Letter:new({ripple = false, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 1.25,scaleTo = 0.75, fadeSpeed = 0.1, colorR= 0,colorG = 0, colorB = 0, colorA = 0, colorToR= 0,colorToG = 0, colorToB = 0,fade= 2.25, delay= 2.25,x = 223,y=200,qx= 218,qy= 87,qw = 47,qh = 84})) 		-- O

	table.insert(buttons,Button:new({debug = false, x = 200, y = 200,  background = false, width = 125, height = 100, label = buttonLabel}))

	table.insert(letters,Letter:new({ripple = false, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 1.25,scaleTo = 0.75, fadeSpeed = 0.1, colorR= 0,colorG = 0, colorB = 0, colorA = 0, colorToR= 0,colorToG = 0, colorToB = 0,fade= 0.5, delay= 0.5,x = -190,y=-170,qx=280,qy=0,qw = 46,qh = 87})) 		-- G
	table.insert(letters,Letter:new({ripple = false, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 1.25,scaleTo = 0.75, fadeSpeed = 0.1, colorR= 0,colorG = 0, colorB = 0, colorA = 0, colorToR= 0,colorToG = 0, colorToB = 0,fade= 0.55, delay= 0.55,x = -158,y=-170,qx= 218,qy= 87,qw = 48,qh = 87})) 	-- O
	table.insert(letters,Letter:new({ripple = false, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 1.25,scaleTo = 0.75, fadeSpeed = 0.1, colorR= 0,colorG = 0, colorB = 0, colorA = 0, colorToR= 0,colorToG = 0, colorToB = 0,fade= 0.6, delay= 0.6, x = -100,y=-170,qx=240,qy=0,qw = 38,qh = 87})) 		-- F
	table.insert(letters,Letter:new({ripple = false, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 1.25,scaleTo = 0.75, fadeSpeed = 0.1, colorR= 0,colorG = 0, colorB = 0, colorA = 0, colorToR= 0,colorToG = 0, colorToB = 0,fade= 0.65, delay= 0.65, x = -70,y=-168,qx= 94,qy = 174,qw = 48,qh = 84})) 	-- U
	table.insert(letters,Letter:new({ripple = false, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 1.25,scaleTo = 0.75, fadeSpeed = 0.1, colorR= 0,colorG = 0, colorB = 0, colorA = 0, colorToR= 0,colorToG = 0, colorToB = 0,fade= 0.7, delay= 0.7, x = -38,y=-168,qx=48,qy=87,qw=46,qh=84})) 	-- L
	table.insert(letters,Letter:new({ripple = false, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 1.25,scaleTo = 0.75, fadeSpeed = 0.1, colorR= 0,colorG = 0, colorB = 0, colorA = 0, colorToR= 0,colorToG = 0, colorToB = 0,fade= 0.75, delay= 0.75, x = -10,y=-168,qx=48,qy=87,qw=46,qh=84})) 	-- L
	table.insert(letters,Letter:new({ripple = false, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 1.25,scaleTo = 0.75, fadeSpeed = 0.1, colorR= 0,colorG = 0, colorB = 0, colorA = 0, colorToR= 0,colorToG = 0, colorToB = 0,fade= 0.8, delay=  0.8, x = 16,y=-166,qx=0,qy=174,qw = 44,qh = 87})) 		-- S
	table.insert(letters,Letter:new({ripple = false, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 1.25,scaleTo = 0.75, fadeSpeed = 0.1, colorR= 0,colorG = 0, colorB = 0, colorA = 0, colorToR= 0,colorToG = 0, colorToB = 0,fade= 0.85, delay= 0.85, x = 46,y=-168,qx=112,qy=0,qw=46,qh=87})) 	-- C
	table.insert(letters,Letter:new({ripple = false, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 1.25,scaleTo = 0.75, fadeSpeed = 0.1, colorR= 0,colorG = 0, colorB = 0, colorA = 0, colorToR= 0,colorToG = 0, colorToB = 0,fade= 0.9, delay=  0.9, rotation = 0.07,x = 80,y=-168,qx= 372,qy = 87,qw = 48,qh = 84})) 	-- R
	table.insert(letters,Letter:new({ripple = false, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 1.25,scaleTo = 0.75, fadeSpeed = 0.1, colorR= 0,colorG = 0, colorB = 0, colorA = 0, colorToR= 0,colorToG = 0, colorToB = 0,fade= 1.0, delay=  1.0, x = 108,y=-168,qx= 202,qw = 37,qh = 87})) 	-- E
	table.insert(letters,Letter:new({ripple = false, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 1.25,scaleTo = 0.75, fadeSpeed = 0.1, colorR= 0,colorG = 0, colorB = 0, colorA = 0, colorToR= 0,colorToG = 0, colorToB = 0,fade= 1.05, delay= 1.05, x =134,y=-168,qx= 202,qw = 37,qh = 87})) 	-- E
	table.insert(letters,Letter:new({ripple = false, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 1.25,scaleTo = 0.75, fadeSpeed = 0.1, colorR= 0,colorG = 0, colorB = 0, colorA = 0, colorToR= 0,colorToG = 0, colorToB = 0,fade= 1.1, delay=  1.1, x =164,y=-168,qx= 170,qy = 87,qw =48,qh = 85})) 	-- N	
	table.insert(letters,Letter:new({ripple = false, speed = 4, amp = 5,scaleSpeed = 0.25, scale = 1.25,scaleTo = 0.75, fadeSpeed = 0.1, colorR= 0,colorG = 0, colorB = 0, colorA = 0, colorToR= 0,colorToG = 0, colorToB = 0,fade= 1.15, delay= 1.15, x =198,y=-170,qx= 450,qy = 0,qw =48,qh = 85})) 	-- ?	
end

function doFullscreen:update(dt)
	updateButtons(dt)
	updateLetters(dt)
end

function doFullscreen:draw()
	love.graphics.push()
	love.graphics.translate(CENTERX,CENTERY)
		lg.setColor(255,255,255)
		lg.rectangle("fill",-CENTERX,-CENTERY,WIDTH,HEIGHT)
		drawdoFullscreen()
		drawButtons()
		drawFade()
	love.graphics.pop()

	lg.setColor(0,0,0)
	for index,value in pairs(debug_text) do
		love.graphics.print(value, 100, 100 + 12*index)
	end

	debug_text = {}	
end

function doFullscreen:keypressed(key, unicode)
	if key == 'escape' then
		love.event.push('quit')
	elseif key == ' ' or key == 'enter' or key == 'y' then
		goFullscreen()
	end
end

function doFullscreen:resize( ... )
	WIDTH = lg.getWidth()
	HEIGHT = lg.getHeight()
	CENTERX = WIDTH/2
	CENTERY = HEIGHT/2
end

function doFullscreen:keyreleased(key)
end

function doFullscreen:mousepressed(x, y, button)
	for i,v in ipairs(buttons) do
		buttons[i]:mousepressed(x,y)
	end
end

function doFullscreen:mousereleased(x, y, button)
	for i,v in ipairs(buttons) do
		buttons[i]:mousereleased(x,y)
	end
end
