sampler TextureSampler : register(s0);

float2 Center = { 0.5, 0.5 }; ///center of the screen (could be any place)
float BlurStart = 1.0f; /// blur offset
float BlurWidth = -0.1; ///how big it should be
int nsamples = 10;

float4 PS_RadialBlur(float2 UV	: TEXCOORD0 ) : COLOR
{
    UV -= Center;
    float4 c = 0;
    for(int i=0; i <nsamples; i++) {
    	float scale = BlurStart + BlurWidth*(i/(float) (nsamples-1));
    	c += tex2D(TextureSampler, UV * scale + Center );
   	}
   	c /= nsamples;
    return c;
}