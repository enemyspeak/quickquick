
extern float radius = 4.0;
extern float min = 0.0;
extern float center_x = 320.0;
extern float center_y = 180.0;

vec4 effect( vec4 color, Image texture, vec2 tc, vec2 pc ) {
	float x = ( pc.x - center_x ) / 127.0;
	float y = ( pc.y - center_y ) / 127.0;
	float a = sqrt( y * y + x * x ) / radius;

	return vec4( color[0], color[1], color[2], clamp( color[3] * ( a ), min, 1.0 ) );
}
