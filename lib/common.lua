
function self:updateShake(dt)
	if self.shakeFactor < 0 then 
		self.shakeFactor = 0
	end
	if self.shakeFactor == 0 then else
		self.shakeCooldown = self.shakeCooldown + dt
		if self.shakeCooldown > self.shakeTime then
			self.shakeCooldown = 0
			local posx = math.random(0,self.shakeFactor)
			local posy = math.random(0,self.shakeFactor)
			local signx = math.random(0,1) -- stay posi or die
			local signy = math.random(0,1)

			if signx == 0 then posx = -posx	end
			if signy == 0 then posy = -posy	end

			flux.to(self,self.shakeTime,{shakeX = posx,shakeY = posy}):ease("linear")
		end
	end
end